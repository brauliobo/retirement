const puppeteer = require('puppeteer')
const _         = require('lodash')
const qs        = require('querystring')

class Page {
  constructor() {
  }

  shortcut(options) {
    this.keyEvent('keydown', options)
    this.keyEvent('keyup', options)
  }

  keyEvent(type, options) {
     var opts = {
        type:         type,
        altKey:       false,
        bubbles:      true,
        cancelBubble: false,
        cancelable:   true,
        metaKey:      false,
        repeat:       false,
        ctrlKey:      false,
        returnValue:  true,
        shiftKey:     false,
        isComposing:  false,
        isTrusted:    true,
        composed:     true,
        defaultPrevented: false,
        code:       'ArrowLeft',
        key:        'ArrowLeft',
        keyCode:    null,
        which:      null,
        charCode:   0,
        detail:     0,
        eventPhase: 3,
        location:   0,
        currentTarget: document,
        target:        document.querySelector('.chart-page'),
        view:          window,
     }
     opts = _.merge(opts, options)
     document.dispatchEvent(new KeyboardEvent(type, opts))
  }
}

function widgetOpts(opts) {
  return {
    container_id: 'tradingview',
    autosize:      true,
    symbol:        opts.symbol,
    interval:      opts.interval,
    timezone:      'America/Sao_Paulo',
    theme:         'dark',
    style:         '1',
    locale:        'en',
    preset:        'mobile',
    enable_publishing:   false,
    hide_side_toolbar:   true,
    allow_symbol_change: true,
  }
}

function widgetUrlOpts(opts) {
  opts = widgetOpts(opts)
  opts.studies = JSON.stringify(opts.studies)
  return opts
}

async function renderWidgetUrl(page, opts) {
  await page.goto(`https://s.tradingview.com/widgetembed?${qs.stringify(widgetUrlOpts(opts))}`)
  const css = `
.item-2KhwsEwE{ font-size: 20px; }
`
  await page.addStyleTag({content: css})
}

async function renderWidget(page, opts) {
  const html = `
<body style='margin: 0;'>
  <div id='tradingview'></div>
  <script type='text/javascript' src='https://s3.tradingview.com/tv.js'></script>
  <script type='text/javascript'>
    window.widget = new TradingView.widget(${JSON.stringify(widgetOpts(opts))})
  </script>
</body>
  `
  await page.setContent(html)
}

async function renderLogged(page) {
  await page.setExtraHTTPHeaders({
    Cookie: process.env.COOKIE,
  })
  await page.goto(`https://www.tradingview.com/chart/VpxmqAIl/`)
}

function link(opts) {
  console.log(`https://s.tradingview.com/widgetembed?${qs.stringify(widgetUrlOpts(opts))}`)
}

async function capture(opts) {
  const browser = await puppeteer.launch({
    args:           ['--no-sandbox', '--disable-setuid-sandbox'],
    executablePath: '/usr/bin/brave',
    headless:       !process.env.LAUNCH,
  })
  const page = await browser.newPage()
  await page.setViewport({
    width:  1600,
    height: 1200,
    //deviceScaleFactor: 2, //fails due to <meta maximum-scale=1.0>
  })

  //renderWidget(page, opts)
  renderWidgetUrl(page, opts)
  //renderLogged(page, opts)

  await page.exposeFunction('pptr_init', () => new Page())
  await page.evaluate(_ => pptr = pptr_init())

  // wait for volume and other fetches/caculations to finish
  await new Promise(resolve => setTimeout(resolve, 3000))
  await page.screenshot({path: opts.output})

  if (!process.env.LAUNCH)
    await browser.close()
}

const argv = process.argv
const cmds = {capture: capture, link: link}
const opts = {
  symbol:   (argv[3] || 'ETHBTC'),
  interval: (argv[4] || '4H').replace('M', ''),
  output:   (argv[5] || 'tv-screenshot.png'),
}
cmds[argv[2]](opts)

