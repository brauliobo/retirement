require_relative 'boot'

require 'httpclient'

require 'active_model/railtie'
require 'active_job/railtie'
# require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
require 'sprockets/railtie'
require 'rails/test_unit/railtie'
require 'sequel_rails/railties/legacy_model_config'

require_relative '../lib/retirement'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Retirement
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Allowed options: :sql, :ruby.
    config.sequel.schema_format = :sql

    # Whether to dump the schema after successful migrations.
    # Defaults to false in production and test, true otherwise.
    config.sequel.schema_dump = true

    config.assets.precompile = %w[
      *.css
      *.js
    ]

    #config.autoload_paths += Dir["#{config.root}/app/**/"]

    config.eager_load = true
    config.eager_load_paths.concat config.autoload_paths
  end
end
