paths = %w(
  .ruby-version
  .rbenv-vars
  config/env
  config/env.local
  config/learner.yml
  db/migrate
)

paths.each{ |path| Spring.watch path }

