DB = Sequel::DATABASES.first

Sequel.extension :core_extensions
DB.extension :pg_array
DB.extension :pg_json

Sequel::Model.plugin :dirty
Sequel::Model.plugin :timestamps
Sequel::Model.plugin :after_initialize

Sequel.application_timezone = :local
Sequel.database_timezone    = :utc

if Rails.env.development?
  DB.sql_log_level = :debug
  #DB.loggers << Logger.new($stdout)
end

