# necessary for riot
Slim::Engine.set_options pretty: true, sort_attrs: false
Slim::PrettyTemplate = Temple::Templates::Tilt Slim::Engine, register_as: :slim, pretty: true
Rails.application.assets.register_engine '.slim', Slim::PrettyTemplate

