require_relative 'ext/module'
require_relative 'ext/object'

require_relative 'ext/integer'
require_relative 'ext/numeric'
require_relative 'ext/string'

require_relative 'ext/enumerable'
require_relative 'ext/array'

require_relative 'ext/moving_average'

