module RaiseWithObject

  attr_accessor :exc_obj

  def raise *args
    args.first.exc_obj = self if args.first
    super
  end

end

