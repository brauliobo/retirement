module FibLevel

  FIB_LEVELS = [
    23.6,
    38.2,
    50.0,
    61.8,
    78.6,
  ]

  def level_in max, min
    range = max - min
    level = (self - max).percent_of range
    level
  end

  def fib_level_in max, min
    level     = self.level_in max, min
    fib_level = FIB_LEVELS.take_while{ |fl| level.abs > fl }.last
    return unless fib_level
    fib_level = -fib_level if level < 0
    fib_level
  end

end

