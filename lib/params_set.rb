module ParamsSet

  def params_set params = {}
    params&.each do |key, value|
      next unless value.present?
      next unless respond_to? key
      instance_variable_set :"@#{key}", value
    end
  end

end
