require 'fib_level'

class Numeric

  include FibLevel

  def growth other
    g  = self / other.to_f - 1
    g *= 100
    g.round 2
  end

  def percent_of other
    p  = self / other
    p *= 100
    p.round 2
  end

  def signif n=4
    BigDecimal.new(self, n).to_f
  end

end
