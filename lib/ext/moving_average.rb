require 'moving_average'

class Array

  # Workaround on https://github.com/bradcater/moving_averages/issues/1
  def smma *args
    return sma(*args) if size == 1
    smoothed_moving_average(*args)
  end

  def sma *args
    return nil if size.zero?
    simple_moving_average(*args)
  end
  def ema *args
    return nil if size.zero?
    exponential_moving_average(*args)
  end
  def wma *args
    return nil if size.zero?
    weighted_moving_average(*args)
  end

end
