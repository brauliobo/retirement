class Array

  def prev_i i, length
    if i <= length then 0 else i - length end
  end

  def rewind_range i, length
    ip = prev_i i, length
    self[ip..i]
  end

  def rewind i, length
    ip = prev_i i, length
    self[ip]
  end

end
