require 'net/http'

class CatchBlock

  ERRORS_TO_RETRY = Set.new [
    HTTPClient::ReceiveTimeoutError,
    HTTPClient::ConnectTimeoutError,
    Net::OpenTimeout,
    Net::ReadTimeout,
    OpenSSL::SSL::SSLError,
    Errno::ECONNRESET,
    Errno::ENETUNREACH,
    SocketError,
    Sequel::PoolTimeout,
  ]

  def self.exec *args, &block
    block.call(*args)
  rescue => e
    raise if e.class == Enumerable::Break
    if e.class.in? ERRORS_TO_RETRY
      puts "Retrying #{e.class}"
      retry
    end

    puts "#{e.class}: #{e.message}: #{e.backtrace.join "\n"}"

    obj = if e.exc_obj then e.exc_obj else block end
    require'pry';obj.binding.pry if ENV['DEBUG']

    Kernel.exit if ENV['EXIT']
  end

end

module Enumerable

  def catch_each &block
    each do |*args|
      CatchBlock.exec(*args, &block)
    end
  end

end

