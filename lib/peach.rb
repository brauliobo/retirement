require_relative 'catch_block'

module Enumerable

  class Break < StandardError; end

  def peach threads: nil, priority: nil, wait: true, &block
    block   ||= -> *args {}
    threads ||= (ENV['THREADS'] || '10').to_i

    return each(&block) if threads == 1

    @break    = false
    pool      = Concurrent::FixedThreadPool.new threads
    # catch_each can't be used as catchblock needs to be used inside pool.post
    ret       = each do |*args|
      pool.post do
        next if @break
        Thread.current.priority = priority if priority
        CatchBlock.exec(*args, &block)
      rescue Break
        @break = true
      end
    end

    pool.shutdown
    pool.wait_for_termination if wait

    ret
  end

  def api_peach threads: nil, priority: nil, &block
    peach(
      threads:  threads || ENV['API_THREADS'],
      priority: priority,
      &block
    )
  end

  def cpu_peach threads: nil, priority: nil, &block
    peach(
      threads:  threads || ENV['CPU_THREADS'],
      priority: ENV['CPU_PRIORITY']&.to_i,
      &block
    )
  end

end
