module Syncronize

  def synchronize *methods
    options = methods.extract_options!
    with    = options[:with] ||= Mutex.new

    methods.each do |method|
      if method_defined? "#{method}_without_synchronization"
        raise ArgumentError, "#{method} is already synchronized. Double synchronization is not currently supported."
      end

      define_method "#{method}_with_synchronization" do |*args, &block|
        with.synchronize do
          send "#{method}_without_synchronization", *args, &block
        end
      end

      alias_method_chain method, :synchronization
    end
  end

end
