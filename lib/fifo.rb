class FIFO < Array

  attr_reader :fifo_length

  def initialize len
    @fifo_length = len
    super
  end

  def push element
    super element
    self.shift if self.length > fifo_length
  end

end
