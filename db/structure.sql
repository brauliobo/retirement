--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: balanced_pairs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.balanced_pairs (
    id integer NOT NULL,
    user_id integer,
    pair_id integer,
    pair_name text,
    quote text,
    base text,
    active boolean DEFAULT true,
    weight double precision,
    base_quote_volume double precision,
    quote_volume double precision,
    base_volume double precision,
    params jsonb DEFAULT '{}'::jsonb,
    triggers jsonb DEFAULT '{}'::jsonb,
    trade_history jsonb DEFAULT '[]'::jsonb,
    data jsonb DEFAULT '{}'::jsonb,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: balanced_pairs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.balanced_pairs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: balanced_pairs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.balanced_pairs_id_seq OWNED BY public.balanced_pairs.id;


--
-- Name: coins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.coins (
    id integer NOT NULL,
    symbol text,
    name text,
    coinmarketcap_id text
);


--
-- Name: coins_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.coins_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: coins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.coins_id_seq OWNED BY public.coins.id;


--
-- Name: live_messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.live_messages (
    chat_id integer NOT NULL,
    id integer NOT NULL,
    updating boolean,
    sent_at timestamp without time zone,
    params jsonb,
    from_msg jsonb,
    balanced_pair_id integer
);


--
-- Name: pair_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pair_orders (
    id integer NOT NULL,
    type text,
    broker text,
    pair text,
    fetched_at timestamp without time zone,
    price double precision,
    base_amount double precision,
    counter_amount double precision
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.pair_orders.id;


--
-- Name: pair_price_analyses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pair_price_analyses (
    id integer NOT NULL,
    broker text,
    pair text,
    price_id integer,
    params jsonb,
    signal text,
    atr double precision,
    trend_up double precision,
    trend_down double precision,
    trend double precision,
    tsl double precision
);


--
-- Name: pair_analyses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pair_analyses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pair_analyses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pair_analyses_id_seq OWNED BY public.pair_price_analyses.id;


--
-- Name: pair_order_analyses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pair_order_analyses (
    id integer NOT NULL,
    broker text,
    pair text,
    fetched_at timestamp without time zone,
    ask_id integer,
    ask_volume double precision,
    ask_growth double precision,
    bid_id integer,
    bid_volume double precision,
    bid_growth double precision,
    bid_ask_ratio double precision,
    trend text,
    signal text,
    data jsonb DEFAULT '{}'::jsonb,
    params jsonb DEFAULT '{}'::jsonb
);


--
-- Name: pair_order_analyses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pair_order_analyses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pair_order_analyses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pair_order_analyses_id_seq OWNED BY public.pair_order_analyses.id;


--
-- Name: pair_prices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pair_prices (
    id integer NOT NULL,
    broker text,
    pair text,
    "time" timestamp without time zone,
    open double precision,
    high double precision,
    low double precision,
    close double precision,
    volume double precision,
    "interval" character varying(3)
);


--
-- Name: pair_prices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pair_prices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pair_prices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pair_prices_id_seq OWNED BY public.pair_prices.id;


--
-- Name: pairs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pairs (
    id integer NOT NULL,
    broker text,
    name text,
    last_price double precision,
    lowest_price_in_3m double precision,
    lowest_price_in_6m double precision,
    lowest_price_in_12m double precision,
    volume_24h numeric,
    active boolean DEFAULT true,
    broker_data jsonb DEFAULT '{}'::jsonb,
    statistics jsonb DEFAULT '{}'::json,
    analyser_params jsonb DEFAULT '{}'::json,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pairs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pairs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pairs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pairs_id_seq OWNED BY public.pairs.id;


--
-- Name: schema_info; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_info (
    version integer DEFAULT 0 NOT NULL
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    filename text NOT NULL
);


--
-- Name: trades; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.trades (
    id integer NOT NULL,
    type text,
    broker text,
    pair text,
    identifier text,
    pending boolean DEFAULT false,
    simulated boolean DEFAULT false,
    price_id integer,
    buy_id integer,
    sell_id integer,
    "time" double precision,
    price double precision,
    amount double precision,
    signal_id text,
    analyser text,
    created_at timestamp without time zone,
    executed_at timestamp without time zone
);


--
-- Name: trades_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.trades_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: trades_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.trades_id_seq OWNED BY public.trades.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    telegram_id numeric,
    tokens jsonb DEFAULT '{}'::jsonb,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: balanced_pairs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.balanced_pairs ALTER COLUMN id SET DEFAULT nextval('public.balanced_pairs_id_seq'::regclass);


--
-- Name: coins id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.coins ALTER COLUMN id SET DEFAULT nextval('public.coins_id_seq'::regclass);


--
-- Name: pair_order_analyses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pair_order_analyses ALTER COLUMN id SET DEFAULT nextval('public.pair_order_analyses_id_seq'::regclass);


--
-- Name: pair_orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pair_orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: pair_price_analyses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pair_price_analyses ALTER COLUMN id SET DEFAULT nextval('public.pair_analyses_id_seq'::regclass);


--
-- Name: pair_prices id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pair_prices ALTER COLUMN id SET DEFAULT nextval('public.pair_prices_id_seq'::regclass);


--
-- Name: pairs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pairs ALTER COLUMN id SET DEFAULT nextval('public.pairs_id_seq'::regclass);


--
-- Name: trades id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.trades ALTER COLUMN id SET DEFAULT nextval('public.trades_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: balanced_pairs balanced_pairs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.balanced_pairs
    ADD CONSTRAINT balanced_pairs_pkey PRIMARY KEY (id);


--
-- Name: coins coins_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.coins
    ADD CONSTRAINT coins_pkey PRIMARY KEY (id);


--
-- Name: live_messages live_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.live_messages
    ADD CONSTRAINT live_messages_pkey PRIMARY KEY (chat_id, id);


--
-- Name: pair_orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pair_orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: pair_price_analyses pair_analyses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pair_price_analyses
    ADD CONSTRAINT pair_analyses_pkey PRIMARY KEY (id);


--
-- Name: pair_order_analyses pair_order_analyses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pair_order_analyses
    ADD CONSTRAINT pair_order_analyses_pkey PRIMARY KEY (id);


--
-- Name: pair_prices pair_prices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pair_prices
    ADD CONSTRAINT pair_prices_pkey PRIMARY KEY (id);


--
-- Name: pairs pairs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pairs
    ADD CONSTRAINT pairs_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (filename);


--
-- Name: trades trades_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.trades
    ADD CONSTRAINT trades_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: coins_symbol_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX coins_symbol_index ON public.coins USING btree (symbol);


--
-- Name: orders_broker_pair_fetched_at_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX orders_broker_pair_fetched_at_index ON public.pair_orders USING btree (broker, pair, fetched_at);


--
-- Name: pair_analyses_params_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pair_analyses_params_index ON public.pair_price_analyses USING btree (params);


--
-- Name: pair_analyses_price_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pair_analyses_price_id_index ON public.pair_price_analyses USING btree (price_id);


--
-- Name: pair_analyses_signal_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pair_analyses_signal_index ON public.pair_price_analyses USING btree (signal);


--
-- Name: pair_order_analyses_broker_pair_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pair_order_analyses_broker_pair_index ON public.pair_order_analyses USING btree (broker, pair);


--
-- Name: pair_order_analyses_signal_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pair_order_analyses_signal_index ON public.pair_order_analyses USING btree (signal);


--
-- Name: pair_order_analyses_trend_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pair_order_analyses_trend_index ON public.pair_order_analyses USING btree (trend);


--
-- Name: pairs_broker_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX pairs_broker_name_index ON public.pairs USING btree (broker, name);


--
-- Name: trades_buy_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX trades_buy_id_index ON public.trades USING btree (buy_id);


--
-- Name: trades_identifier_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX trades_identifier_index ON public.trades USING btree (identifier);


--
-- Name: trades_pending_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX trades_pending_index ON public.trades USING btree (pending);


--
-- Name: trades_price_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX trades_price_id_index ON public.trades USING btree (price_id);


--
-- Name: trades_sell_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX trades_sell_id_index ON public.trades USING btree (sell_id);


--
-- Name: trades_simulated_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX trades_simulated_index ON public.trades USING btree (simulated);


--
-- Name: trades_type_broker_pair_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX trades_type_broker_pair_index ON public.trades USING btree (type, broker, pair);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;
INSERT INTO "schema_migrations" ("filename") VALUES ('20170320235342_create_orders.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170321225922_create_pair_prices.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170327231847_create_trades.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170402191637_create_pair_analysis.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170404215450_add_signal_id_to_trades.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170405231131_create_pairs.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170405231200_add_period_to_pair_prices.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170415130656_rename_pair_analysis_to_pair_price_analysis.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170415130850_rename_order_table.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170415131036_create_pair_order_analysis.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170429140726_add_active_to_pair.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170430171506_add_indexes_to_pair_order_analysis.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170430195912_fix_unique_index_on_pair_prices.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('202031203337_add_broker_data_to_pairs.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170617210859_add_statistics_to_pairs.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20170816202625_create_coins_table.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20171225204900_add_analyser_params_to_pairs.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20171231203337_add_created_at_to_trades.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20210623204200_add_updated_created_at_to_pairs.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20210707143600_create_live_message.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20210710214000_change_periods_in_pair_prices.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20210721030000_create_users.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20210804202500_create_balanced_pair.rb');
INSERT INTO "schema_migrations" ("filename") VALUES ('20210807085300_add_balanced_pair_id_to_live_messages.rb');