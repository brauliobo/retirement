Sequel.migration do
  change do
    create_table :coins do
      primary_key :id
      String :symbol
      String :name
      String :coinmarketcap_id

      index :symbol, unique: true
    end
  end
end
