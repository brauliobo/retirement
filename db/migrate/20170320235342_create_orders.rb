Sequel.migration do
  change do

    create_table :orders do
      primary_key :id
      String :type

      String :broker
      String :pair
      Time :fetched_at

      String :identifier

      Float :price
      Float :base_amount
      Float :counter_amount

      index [:broker, :pair, :fetched_at]
    end

  end
end
