Sequel.migration do
  change do
    create_table :pairs do
      primary_key :id

      String :broker
      String :name

      Float :last_price
      Float :lowest_price_in_3m
      Float :lowest_price_in_6m
      Float :lowest_price_in_12m

      String :volume_24h

      index [:broker, :name]
    end
  end
end
