Sequel.migration do
  change do
    alter_table :pairs do
      add_column :broker_data, :jsonb, default: {}.to_json
    end
  end
end
