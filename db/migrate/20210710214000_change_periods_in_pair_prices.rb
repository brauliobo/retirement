Sequel.migration do
  change do
    alter_table :pair_prices do
      drop_column :period
      add_column :interval, String, size: 3
    end
  end
end
