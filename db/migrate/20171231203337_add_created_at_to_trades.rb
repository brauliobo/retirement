Sequel.migration do
  change do
    alter_table :trades do
      add_column :analyser, String
      add_column :created_at, Time
      add_column :executed_at, Time
    end
  end
end
