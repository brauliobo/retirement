Sequel.migration do
  change do
    create_table :pair_order_analyses do
      primary_key :id

      String :broker
      String :pair
      Time :fetched_at

      Integer :ask_id
      Float :ask_volume
      Float :ask_growth

      Integer :bid_id
      Float :bid_volume
      Float :bid_growth

      Float :bid_ask_ratio

      String :trend
      String :signal

      jsonb :data, default: {}.pg_jsonb
      jsonb :params, default: {}.pg_jsonb
    end
  end
end
