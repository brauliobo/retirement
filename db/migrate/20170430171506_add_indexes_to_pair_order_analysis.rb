Sequel.migration do
  change do
    alter_table :pair_order_analyses do
      add_index [:broker, :pair]
      add_index :trend
      add_index :signal
    end
  end
end
