Sequel.migration do
  change do
    create_table :users do
      primary_key :id
      Numeric :telegram_id

      column :tokens, :jsonb, default: {}.to_json

      Time :created_at
      Time :updated_at

    end
  end
end
