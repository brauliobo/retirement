Sequel.migration do
  change do
    alter_table :live_messages do
      add_column :balanced_pair_id, Integer
    end
  end
end
