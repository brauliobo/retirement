Sequel.migration do
  change do
    alter_table :pair_prices do
      add_column :period, Integer
      add_index [:period]
    end
    PairPrice.dataset.update period: 5
  end
end
