Sequel.migration do
  change do
    alter_table :pair_prices do
      drop_index :broker_pair_time
      add_index [:broker, :pair, :period, :time], unique: true
    end
  end
end
