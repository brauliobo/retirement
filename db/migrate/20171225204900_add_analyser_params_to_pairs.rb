Sequel.migration do
  up do
    alter_table :pairs do
      add_column :analyser_params, :jsonb, default: {}.pg_json
    end
  end

  down do
    alter_table :pairs do
      drop_column :analyser_params
    end
  end
end
