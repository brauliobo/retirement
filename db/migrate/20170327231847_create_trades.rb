Sequel.migration do
  change do
    create_table :trades do
      primary_key :id
      String :type

      String :broker
      String :pair

      String :identifier
      Boolean :pending, default: false
      Boolean :simulated, default: false

      Integer :price_id
      Integer :buy_id
      Integer :sell_id

      Float :time
      Float :price
      Float :amount

      index [:identifier]
      index [:type, :broker, :pair]

      index [:pending]
      index [:simulated]

      index [:price_id]
      index [:buy_id]
      index [:sell_id]
    end
  end
end
