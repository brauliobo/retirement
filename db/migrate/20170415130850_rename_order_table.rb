Sequel.migration do
  change do
    drop_column :orders, :identifier
    rename_table :orders, :pair_orders
  end
end
