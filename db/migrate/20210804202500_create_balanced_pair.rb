Sequel.migration do
  change do
    create_table :balanced_pairs do
      primary_key :id

      Integer :user_id
      Integer :pair_id

      String :pair_name
      String :quote
      String :base

      TrueClass :active, default: true

      Float :weight
      # ETHUSDT -> base ETH, quote USDT
      Float :base_quote_volume # 50 USDT in ETH
      Float :quote_volume # 50 USDT
      Float :base_volume  # 0.0161777 ETH

      column :params, :jsonb, default: {}.to_json
      column :triggers, :jsonb, default: {}.to_json

      column :trade_history, :jsonb, default: [].to_json
      column :data, :jsonb, default: {}.to_json

      Time :created_at
      Time :updated_at
    end
  end
end
