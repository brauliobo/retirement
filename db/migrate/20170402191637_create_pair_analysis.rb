Sequel.migration do
  change do
    create_table :pair_analyses do
      primary_key :id

      String :broker
      String :pair
      Integer :price_id

      jsonb :params
      String :signal

      Float :atr
      Float :trend_up
      Float :trend_down
      Float :trend
      Float :tsl

      index [:price_id]
      index [:params]
      index [:signal]
    end
  end
end
