Sequel.migration do
  up do
    alter_table :pairs do
      add_column :statistics, :jsonb, default: {}.pg_json
    end
  end

  down do
    alter_table :pairs do
      drop_column :statistics
    end
  end
end
