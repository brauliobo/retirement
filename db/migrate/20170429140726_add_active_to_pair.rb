Sequel.migration do
  change do
    alter_table :pairs do
      add_column :active, :boolean, default: true
    end
  end
end
