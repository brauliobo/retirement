Sequel.migration do
  change do
    create_table :live_messages do
      Integer :chat_id
      Integer :id
      primary_key [:chat_id, :id]

      TrueClass :updating
      Time :sent_at
      column :params, :jsonb

      column :from_msg, :jsonb

    end
  end
end
