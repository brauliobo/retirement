Sequel.migration do
  change do
    rename_table :pair_analyses, :pair_price_analyses
  end
end
