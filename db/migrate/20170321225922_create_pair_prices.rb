Sequel.migration do
  change do

    create_table :pair_prices do
      primary_key :id

      String :broker
      String :pair
      Time :time

      Float :open
      Float :high
      Float :low
      Float :close

      Float :volume

      index [:broker, :pair, :time], unique: true
    end

  end
end
