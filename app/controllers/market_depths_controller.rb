class MarketDepthsController < ApplicationController

  def index
    @pair    = Pair.where(
      broker: params[:broker] || Pair.default.broker,
      name:   params[:pair]   || Pair.default.name,
    ).first
  end

end
