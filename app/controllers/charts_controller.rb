class ChartsController < ApplicationController

  def index
    @pair    = Pair.where(
      broker: params[:broker] || Pair.default.broker,
      name:   params[:pair]   || Pair.default.name,
    ).first
    @start   = params[:days]&.to_i&.days&.ago || params[:hours]&.to_i&.hours&.ago || Pair.prices_analysis_hours.hours.ago

    @range   = (params[:range] || Analyser::PricesBase.range).to_sym
    @factor  = params[:factor]&.to_f
    @pd      = params[:pd]&.to_f
    @aparams = {
      range:  @range,
      factor: @factor,
      pd:     @pd,
    }

    @analyses = @pair.analyse_prices name: Pair.prices_analysis_method, sync: true, **@aparams

    # do analysis with more data but show only 24h data
    @analyses.select!{ |p| p.time >= @start }
  end

end
