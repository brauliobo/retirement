window.charts = {

  chart: null,

  load: (params) => {
    charts.chart = new CanvasJS.Chart('chart', {})
    charts.render(params.pair, params.prices)

    App.cable.subscriptions.create(
      {channel: 'TickersChannel', broker: params.pair.broker, pair: params.pair.name},
      {received: (ticker) => {
        var pair = params.pair
        pair.last_price = ticker.last_price
        charts.chart.options.title.text = `${pair.broker}/${pair.name} ${pair.last_price || ''}`
        charts.chart.render()
      }}
    )
  },

  render: (pair, prices) => {
    _.each(prices, (p) => p.time = Date.parse(p.price.time))

    var options = {
      title: {
        text: `${pair.broker}/${pair.name} ${pair.last_price || ''}`,
      },
      axisY: {
        includeZero: false,
        //prefix: '$',
      },
      axisX: {
        interval: 1*60*60*1000,
        labelFormatter: (t) => moment(t.value).format('H:mm')
      },
      data: [
        {
          type:        'candlestick',
          risingColor: 'red',
          dataPoints:  _.map(prices, (p, i) => {
            return {
              x: p.time,
              y: [p.price.open, p.price.high, p.price.low, p.price.close],
              toolTipContent: `
                <b>Time:</b>  ${moment(p.time).format('H:mm')}<br>
                <b>Open:</b>  ${p.price.open}<br>
                <b>High:</b>  ${p.price.high}<br>
                <b>Low:</b>   ${p.price.low}<br>
                <b>Close:</b> ${p.price.close}<br>
              `,
            }
          }),
        },
        {
          type:  'line',
          color: '#bbb',
          //lineThickness: 3,
          dataPoints: _.map(prices, (p, i) => {
            return {
              x: p.time,
              y: p.tsl,
              color: !p.signal ? '#bbb' : p.signal == 'up' ? 'green' : 'red',
              markerSize: p.signal ? 10 : 'auto',
              toolTipContent: `
                <b>Time:</b>   ${moment(p.time).format('H:mm')}<br>
                <b>Tsl:</b>    ${p.tsl}<br>
                <b>Signal:</b> ${p.signal}<br>
              `,
            }
          }),
        },
      ],
    }

    charts.chart.options = options
    charts.chart.render()
  },

}
