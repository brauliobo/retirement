class TickersChannel < ApplicationCable::Channel

  def subscribed
    stream_from "tickers/#{params[:broker]}/#{params[:pair]}"
  end

end
