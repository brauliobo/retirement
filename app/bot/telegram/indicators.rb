module Bot
  class Telegram
    module Indicators

      def rsi_value data, params
        TechnicalAnalysis::Rsi.calculate(data, period: params.periods, price_key: :close).first.rsi.round 2
      end

    end
  end
end
