class Bot::Telegram
  class Balance < Command
    module Proposal

    def propose
      @interval = params.triggers&.interval
      @interval = ChronicDuration.parse @interval if @interval
      @weight   = params.weight

      params.merge!(
        quote:        pair.quote,
        base:         pair.base,
        quote_volume: @quote_volume,
        base_volume:  @base_volume,
        bq_volume:    @bq_volume,
        weight:       "#{params.weight*100}%",
      )
      stat_msg  = CONFIRMATION % params
      #stat_msg << assets_allocation

      status.edit text: stat_msg, reply_markup: confirmation_markup
    end

    def create_available
      bp_create
      info
    end

    def create_buy
      status.clear
      status.set 'Sending order'
      #order = user.buy pair, quote_volume: @quote_volume
      order = nil
      status.add "Order sent #{order.inspect}"

      bp_create order
      info
    end

    def cancel
      delete_message lm.from_msg, lm.from_msg.id, wait: nil
      delete_message lm.from_msg, lm.id, wait: nil
      lm.update updating: false
    end

    protected

    def confirmation_markup
      btns = []

      if @base_asset and @base_asset.available >= @bq_volume
        btns << ::Telegram::Bot::Types::InlineKeyboardButton.new(text: use_available_message, callback_data: cbdata.merge(action: :create_available).to_json)
      end
      if @quote_asset and @quote_asset.available >= @quote_volume + @base_volume
        btns << ::Telegram::Bot::Types::InlineKeyboardButton.new(text: buy_message, callback_data: cbdata.merge(action: :create_buy).to_json)
      end

      btns << ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Cancel', callback_data: cbdata.merge(action: :cancel).to_json)

      ::Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: btns
    end

    def use_available_message
      "Balance available %{bq_volume} %{base} and %{base_volume} %{quote}" % @params
    end

    def buy_message
      "Buy %{bq_volume} %{base} (%{quote_volume} %{quote}) and balance with %{base_volume} %{quote}" % @params
    end

    private

    def bp_create order = nil
      trade_history = if order then [order] else [] end
      @bp = BalancedPair.create(
        user_id:       user.id,
        pair_id:       pair.id,
        pair_name:     pair.name,
        quote:         pair.quote,
        base:          pair.base,
        weight:        @weight,
        quote_volume:  @quote_volume,
        base_volume:   @base_volume,
        base_quote_volume: @bq_volume,
        params:        params,
        trade_history: trade_history,
      )
      lm.update balanced_pair_id: @bp.id
      @bp
    end

  end
end

end
