require_relative 'balance/proposal'

module Bot
  class Telegram
    class Balance < Command

      attr_reader :params, :msg
      attr_reader :pair
      attr_reader :bp

      self.cb_on_same_user = true

      include Proposal

      DEFAULT_PARAMS = {
        weight:     0.5,
        volume:     11,
        min_volume: 11,
        triggers:   {},
      }

      def initialize bot, msg, cmd, cargs, lm: nil
        super
        @params = SymMash.new YAML.load "{#{cargs}}"
        @params.reverse_merge! DEFAULT_PARAMS
        @bp     = lm&.balanced_pair
        @pair   = Pair.find broker: 'binance', name: params.pair
      end

      CONFIRMATION = <<-EOS
*Pair*: %{pair}
*Weight percentage*: %{weight}
EOS

      def info
        return unless bp

        lp = pair.ticker.lastPrice.to_f

        text = <<-EOS
Last ratio: #{bp.quote_volume} #{bp.quote} #{bp.base_volume} #{bp.base}

#{bp.base} price: #{lp} #{bp.quote}
Current ratio: #{bp.current_quote_volume price: lp} in #{bp.base} and #{bp.quote_volume} #{bp.quote}
EOS
        status.edit text: text, reply_markup: tracking_markup
      end

      def rebalance
      end

      def stop
        delete_message msg, lm.id, wait: nil
        lm.update updating: false
        bp.update active: false
      end

      def validate
        return status.error "Can't find pair #{pair.name}" if not pair
        return status.error "Weight must be between 0 and 1" if not params.weight.in? 0..1

        @quote_asset  = user.assets[pair.quote]
        @base_asset   = user.assets[pair.base]
        @quote_volume = params.volume * params.weight
        @base_volume  = params.volume * (1-params.weight)
        @bq_volume    = (@base_volume / pair.ticker.lastPrice.to_f).round 6
        return status.error "Not enough #{pair.quote}" if @quote_asset and @quote_asset.available < @quote_volume

        true
      end

      def run
        return unless user_token_present?

        status.start 'validating'
        return info if @bp
        return unless validate == true
        propose
      end

      protected

      def tracking_markup
        btns = []

        btns << ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Update', callback_data: cbdata.merge(action: :info).to_json)
        btns << ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Rebalance', callback_data: cbdata.merge(action: :rebalance).to_json)
        btns << ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Stop', callback_data: cbdata.merge(action: :stop).to_json)

        ::Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: btns
      end

      def assets_allocation
        m  = "\n*Assets allocation*\n"
        m << user.assets.map do |asset, a|
          next unless asset.in? [pair.base, pair.quote]
          "#{asset}"
        end.compact.join("\n")
        m
      end

    end
  end
end
