require 'fifo'

module Bot
  class Telegram
    class Command
      class Status

        class_attribute :line_limit
        self.line_limit = 5

        class_attribute :accum_thld
        self.accum_thld = 10

        attr_reader :cmd
        attr_reader :params
        attr_reader :body

        delegate_missing_to :cmd

        def initialize cmd, **params
          @cmd    = cmd
          @fifo   = FIFO.new line_limit
          @accum  = 0
          @params = SymMash.new params
        end

        def start line, **params
          @params.merge! params
          return if cmd.lm
          @body = line
          resp  = cmd.send_message msg, line, **@params
          lm_create resp.result.message_id
        end

        def body= body
          @body = body
          write
        end

        def edit text: nil, **params
          @params.merge! params
          @body = text
          edit_message msg, cmd.lm.id, text: text, **@params
        end

        def set line
          clear
          @fifo << line
          write last_updated: false
        end
        def error line
          stext  = "#{Emoji.find_by_alias('x').raw} "
          stext << "#{line}"
          set stext
        end

        def add line
          @fifo.push line
          update
        end
        def end
          clear
          write
        end
        def clear
          @fifo.clear
        end

        protected

        def update
          return if (@accum += 1) < accum_thld
          @accum = 0
          write
        end

        def write last_updated: true
          stext  = "#{@body}\n\n"
          stext << "#{@fifo.join "\n"}"
          stext << "\n\nLast updated: #{Time.now.to_formatted_s :db}" if last_updated
          cmd.edit_message msg, cmd.lm.id, text: stext, **@params
        end

        def lm_create message_id, params: {}
          cmd.lm ||= LiveMessage.create(
            chat_id:  msg.chat.id,
            id:       message_id,
            from_msg: msg.instance_values,
            updating: true,
            params:   params,
            sent_at:  Time.now,
          )
        end

      end
    end
  end
end
