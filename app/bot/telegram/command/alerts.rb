require_relative 'status'

module Bot
  class Telegram
    class Alerts < Command

      attr_reader :params

      LIMIT          = 20
      DEFAULT_PARAMS = {
        base:   'USDT',
        volume: {min: 1_000_000},
        ints:   {
          '5m': {RSI: {max: 35, periods: 14}},
        },
      }

      def initialize bot, msg, cmd, cargs, lm: nil
        super
        @params = SymMash.new YAML.load "{#{cargs}}"
        @params = SymMash.new DEFAULT_PARAMS if params.blank?
        @rank   = []
        @status.params.parse_mode = 'HTML'
      end

      def run
        loop do
          break if @stop
          @rank.clear
          @status.edit text: "#{broker.name}: iterating"

          broker.pairs.peach do |p|
            next if params.base and !p.name.ends_with? params.base
            next false if unsatified_conds? p.volume_24h, params.volume
            break if @stop
            check_send_pair p
          rescue
            raise Enumerable::Break
          end
          sleep 30.seconds
        end

      ensure
        status.end
      end

      def stop
        @lm.update updating: false
        @status.edit type: 'reply_markup', reply_markup: reply_markup
        @stop = true
      end

      def reply_markup
        btns = []
        btns << ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Cancel update', callback_data: cbdata.merge(action: :stop).to_json) unless @stop
        ::Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: btns
      end

      def prepare
        status.start "#{broker.name}: fetching pairs"
        @status.params.reply_markup = reply_markup
      end

      def unsatified_conds? v, specs
        return true unless v
        min,max = specs.values_at :min, :max
        return "min: #{min}, max #{max}" if max and min and v < min and v > max
        return "min: #{min}" if min and v < min
        return "max: #{max}" if max and v > max
        false
      end

      def check_send_pair p
        params = @params.dup
        params.ints.select! do |int, inds|
          periods = inds.map{ |ind, ind_params| 5 * ind_params.periods }.max
          data    = broker.pair_prices p, int, periods

          inds.each do |ind, ind_params|
            value = send "#{ind.downcase}_value", data, ind_params

            conds = unsatified_conds? value, ind_params
            base  = "#{p.name}/#{int}/#{ind}: #{value}"
            next status.add "#{base} (#{conds})" if conds

            ind_params.value = value.round 1
            ind_params.id    = "#{p.name}-#{int}-#{ind}-#{ind_params.inspect}"
          end

          next unless inds.all?{ |ind, ind_params| ind_params.value }
          status.add "#{p.name} accepted"
          true
        end

        return if params.ints.blank?

        #report_separate p, params
        @rank << SymMash.new(
          pair: p,
          ints: params.ints,
        )

        status.body = ranked_data
      end

      def ranked_data
        @rank.sort_by! do |p|
          first_int = p.ints.values.first
          first_int.values.first.value
        end
        rank = @rank.first LIMIT
        rank.map! do |p|
          v = SymMash.new pair: p.pair.name, vol: sprintf('%.2e', p.pair.volume_24h)

          p.ints.each do |int, inds|
            inds.each do |ind, ind_params|
              v[:"#{int}-#{ind}"] = ind_params.value
            end
          end
          v
        end
        "<pre>#{Tabulo::Table.new(rank, *rank.first.keys).pack}</pre>"
      end

      def report_separate p, params
        text = "*#{p.name}*"
        params.ints.each do |int, inds|
          inds.each do |ind, ind_params|
            next unless ind_params.id
            text += "\n*#{int} #{ind.upcase}*: #{ind_params.value}"
          end
        end

        btns  = [
          ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Buy',  callback_data: 'buy'),
          ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Sell', callback_data: 'sell'),
        ]
        rkm   = ::Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: btns

        #fint  = params.ints.keys.first
        #fintp = params.ints[fint]
        #photo = capture_upload p, fint, fintp.id
        #resp  = send_message msg, text, type: 'photo', photo: photo, reply_markup: rkm
        #File.unlink "tmp/#{fintp.id}"

        send_message msg, text, reply_markup: rkm, delete: 2.minutes
      end

    end
  end
end
