module Bot
  class Telegram
    module CaptureHelpers

      def generate_media int_data
        media = int_data.peach(threads: int_data.size).each.with_object({}) do |id, h|
          h[id.media] = capture_upload
        end

        media[:media] = media.map do |f, io|
          ::Telegram::Bot::Types::InputMediaPhoto.new media: "attach://#{f}"
        end

        media
      end

      def capture_upload pair, interval, filename
        filename = "tmp/#{filename}"
        capture pair.name, interval, filename
        ctype = MIME::Types.type_for(filename).first.content_type
        h[id.media] = Faraday::UploadIO.new filename, ctype
      end

      def capture symbol, interval, output
        system "node tradingview-capture.js capture #{symbol} #{interval} #{output}"
        output
      end

    end
  end
end
