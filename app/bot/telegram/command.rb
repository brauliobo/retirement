require_relative 'indicators'

module Bot
  class Telegram
    class Command

      class InvalidCommand < StandardError; end

      REGEXP = /^\/(\w+) *(.*)/
      LIST   = SymMash.new(
        start: {},
        help:  {},

        binance_token: {
          args: /(\w+) +(\w+)/,
          help: '<key> <secret>',
        },
        alerts: {
        },
        balance: {
          help: 'pair: ETHUSDT, weight: 0.5, base_volume: 100, triggers: {interval: 10 minutes}',
        },

        # admin
        exit:  {},
        exec:  {},
        # test
        test:  {},
        rmk:   {},
      )

      include Indicators

      attr_reader :bot, :msg, :cmd, :cargs
      attr_reader :dec
      attr_reader :status
      attr_accessor :lm

      class_attribute :cb_on_same_user
      self.cb_on_same_user = false

      delegate_missing_to :bot

      def initialize bot, msg, cmd, cargs, lm: nil
        @bot    = bot
        @msg    = msg
        @cmd    = cmd
        @cargs  = cargs
        @lm     = lm
        @dec    = LIST[cmd.to_sym]
        @status = Status.new self
      end

      def parse_args
        args = dec.args.match @cargs
        raise InvalidCommand unless args
        args = args.captures.map(&:presence)

      rescue InvalidCommand
        send_message msg, "Incorrect format, usage is:\n#{help_cmd cmd}"
      end

      def access_denied?
        return status.error 'Different user' unless lm_same_user? if cb_on_same_user
        false
      end

      def prepare
      end
      def run
        return send "cmd_#{cmd}", *parse_args if dec.args
        send "cmd_#{cmd}"
      end

      def cmd_binance_token key, secret, **params
        user = User.find_or_create telegram_id: msg.from.id
        binance = user.tokens[:binance] ||= SymMash.new
        binance.key    = key
        binance.secret = secret
        user.save

        send_message msg, 'Token saved', delete_both: (0 if msg.chat.id != msg.from.id)
      end

      def cmd_resume **params
        resume_updating
      end

      def cmd_exec *args
        return unless from_admin? msg
        ret = instance_eval(args.join(' ')).inspect
        send_message msg, ret, delete_both: 30.seconds, parse_mode: 'HTML'
      end
      def cmd_exit msg, *args, **params
        return unless from_admin? msg
        @exit = true
      end

      def cmd_rmk msg, *args, **params
        kb = ::Telegram::Bot::Types::ReplyKeyboardRemove.new remove_keyboard: true
        send_message msg, 'finish', reply_markup: kb
      end

      def cmd_test msg, text, **params
        btns = [
          ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'RSI', callback_data: 'test'),
          ::Telegram::Bot::Types::InlineKeyboardButton.new(text: 'RSI2', callback_data: 'test'),
        ]
        rkm = ::Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: btns
        send_message msg, 'text', parse_mode: 'MarkdownV2', reply_markup: rkm
      end

      protected

      def lm_same_user?
        return true unless lm
        msg.from.id == lm.from_msg.from.id
      end

      def user_token_present?
        return true if user.tokens['binance'].present?

        send_message msg, 'User token not, use /binance\_token first', delete_both: true
        false
      end

      def user
        @user ||= User.find_or_create telegram_id: msg.from.id
      end

      def chat_id
        msg.chat_id
      end

      def cbdata
        SymMash.new lm_id: lm.id
      end

    end
  end
end
