require 'bot'
require 'telegram/bot'
require_relative './telegram/helpers'
require_relative './telegram/capture_helpers'

module Bot
  class Telegram

    include Helpers
    include CaptureHelpers

    extend Limiter::Mixin
    # 30req/s not enough...
    limit_method :send_message,   rate: 60, interval: 60
    limit_method :edit_message,   rate: 60, interval: 60
    limit_method :delete_message, rate: 60, interval: 60
    # below 30/s
    limit_method :send_message,   rate: 10, interval: 1
    limit_method :edit_message,   rate: 10, interval: 1
    limit_method :delete_message, rate: 10, interval: 1

    attr_reader :bot
    attr_reader :broker, :client

    class_attribute :cmd_map
    self.cmd_map = {}

    def initialize
      @broker = Broker::Binance.new
      @client = Broker::Binance.public_client
    end

    def start pair = nil
      ::Telegram::Bot::Client.run ENV['TOKEN'], logger: Logger.new(STDOUT) do |bot|
        @bot = bot
        resume_updating
        @bot.listen do |msg|
          Thread.new do
            case msg
            when ::Telegram::Bot::Types::CallbackQuery
              react_callback msg
            when ::Telegram::Bot::Types::Message
              react msg
            end
          end
          Thread.new{ sleep 1 and abort } if @exit # wait for other msg processing and trigger systemd restart
        end
      end
    end

    def resume_updating
      LiveMessage.where(updating: true).each do |lm|
        Thread.new do
          react lm.from_msg, lm: lm
        end
      end
    end

    def react_callback msg
      puts "callback: #{msg.data}"

      data = SymMash.new JSON.parse msg.data
      return unless cmd = cmd_map[data.lm_id]
      return if cmd.access_denied?
      cmd.send data.action

    rescue => e
      report_error cmd.lm.from_msg, e
    end

    def react msg, lm: nil
      puts "message: #{msg.text}"

      cmd,args = msg.text.match(Command::REGEXP)&.captures
      return unless cmd

      klass = Telegram.const_get cmd.camelize rescue nil
      klass = Command unless klass&.superclass == Command

      cmd = klass.new self, msg, cmd, args, lm: lm
      cmd.prepare
      cmd_map[cmd.lm&.id] = cmd
      cmd.run

    rescue => e
      report_error msg, e
    end

  end
end
