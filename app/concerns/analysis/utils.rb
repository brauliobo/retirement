module Analysis
  module Utils

    def hl2
      (@p.high + @p.low) / 2
    end

    def range
      @p.high - @p.low
    end

    def nz v, d
      if v then v else d end
    end

    def cross x, y
    end

  end
end
