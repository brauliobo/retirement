module Analysis
  class BaseResult < Array

    attr_reader :analyser
    attr_reader :pair

    attr_accessor :buy_signal
    attr_accessor :sell_signal
    attr_accessor :signal
    attr_accessor :price

    attr_accessor :signals
    attr_accessor :turning
    attr_accessor :trend
    attr_accessor :alignment

    attr_accessor :gains
    attr_accessor :losses

    delegate :print_report, to: :analyser

    def initialize array, analyser:
      super array
      @analyser = analyser
      @pair     = analyser.pair
      @signals  = []
    end

  end
end
