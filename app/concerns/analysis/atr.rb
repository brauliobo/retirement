module Analysis
  ##
  # https://en.wikipedia.org/wiki/Average_true_range
  #
  module Atr

    include Navigation

    def atr l:, i: @i
      ip       = ip i
      pp       = @d[ip]
      pp.atr ||= tr

      p     = @d[i]
      p.atr = (pp.atr * (l-1) + tr) / l
    end

    def tr i: @i
      p  = @d[i]
      ip = ip i
      pp = @d[ip]

      [
        (p.high - p.low),
        (p.high - pp.close).abs,
        (p.low  - pp.close).abs,
      ].max
    end

  end
end
