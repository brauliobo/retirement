module Analysis
  class Trades

    def self.summary
      Buy.sold.map do |buy|
        {
          buy:         buy,
          sell:        buy.sell,
          price_ratio: buy.sell.price.growth(buy.price),
        }
      end
    end

  end
end
