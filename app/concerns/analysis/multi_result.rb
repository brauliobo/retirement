module Analysis
  class MultiResult < SymMash

    attr_reader :analyser
    attr_reader :pair

    attr_accessor :buy_signal
    attr_accessor :sell_signal
    attr_accessor :signal

    def initialize hash, analyser:
      super hash
      @analyser = analyser
      @pair     = analyser.pair
    end

  end
end
