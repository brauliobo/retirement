module Analysis
  class PricesResult < BaseResult

    attr_accessor :start

    def signals buy_id: 'up', sell_id: 'down'
      buy,sell = nil
      each.with_object({}) do |a, signals|
        buy,sell = a,nil if a.signal == buy_id
        sell = a if buy and a.signal == sell_id

        if buy and sell
          signals[buy] = sell
          buy = nil
        end
      end
    end

    def percentages
      signals.map do |buy, sell|
        sell.price.low.growth(buy.price.high)
      end
    end

    def ipercentages
      signals(buy_id: 'down', sell_id: 'up').map do |buy, sell|
        sell.price.low.growth(buy.price.high)
      end
    end

  end
end
