module Poloniex
  module Signature

    extend ActiveSupport::Concern

    class NoSecretError < StandardError; end

    def signature params:
      raise NoSecretError if ENV['POLONIEX_SECRET'].blank?

      digest = OpenSSL::Digest.new 'sha512'
      sign   = OpenSSL::HMAC.hexdigest digest, ENV['POLONIEX_SECRET'], params_encoded(params)

      sign
    end

    def params_encoded params
      URI.encode_www_form params
    end
  end

end
