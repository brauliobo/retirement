module Poloniex
  module Api

    def get url
      response = HttpClient.new.get url
      data     = JSON.parse response.body
      raise data['error'] if data.is_a? Hash and data['error']
      data
    end

  end
end
