require 'net/http'

module Poloniex
  module TradingApi

    include Signature

    Url = 'https://poloniex.com/tradingApi'

    class NoKeyError < StandardError; end

    def trading_post command:, params:
      raise NoKeyError if ENV['POLONIEX_KEY'].blank?

      params.merge!(
        nonce:   nonce,
        command: command,
      )

      uri    = URI.parse Url
      client = Net::HTTP.new uri.host, uri.port
      client.use_ssl = true

      request = Net::HTTP::Post.new uri.path
      request['Key']  = ENV['POLONIEX_KEY']
      request['Sign'] = signature params: params
      request.body = params_encoded params

      response = client.request request
      data     = JSON.parse response.body
      raise data['error'] if data.is_a? Hash and data['error']
      data
    end

    synchronize :trading_post

    def nonce
      DateTime.now.strftime '%Q'
    end

  end
end
