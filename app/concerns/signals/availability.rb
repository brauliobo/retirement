module Signals
  module Availability

    extend ActiveSupport::Concern

    included do
      class_attribute :max_percentage_per_trade
      self.max_percentage_per_trade = ENV['AVAILABLE_MAX_PERCENTAGE_PER_TRADE'].to_f

      class_attribute :max_available_to_buy
      self.max_available_to_buy = Broker::Base.available_in_base_pair * self.max_percentage_per_trade
    end

  end
end
