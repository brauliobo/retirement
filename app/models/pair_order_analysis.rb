class PairOrderAnalysis < ApplicationModel

  many_to_one :ask, class: PairAsk
  many_to_one :bid, class: PairOrder

  plugin :serialization, :json, :data

  delegate :price, to: :ask, prefix: :ask
  delegate :price, to: :bid, prefix: :bid

  tp.set self, *[
    :ask_price,
    :ask_volume,
    :ask_growth,
    :bid_price,
    :bid_volume,
    :bid_growth,
    :bid_ask_ratio,
    :trend,
    :signal,
    :derivative,
  ]

  dataset_module do

    def last_fetched_at
      self
        .order(Sequel.desc :fetched_at)
        .limit(1)
        .select_map(:fetched_at)
        .first
    end

    def latest
      where fetched_at: self.last_fetched_at
    end

  end

  def derivative
    data[:derivative]
  end

  protected

  def before_validation
    order = ask || bid
    self.broker     = order.broker
    self.pair       = order.pair
    self.fetched_at = order.fetched_at
    super
  end

  def validate
    super
    validates_presence [:broker, :pair, :fetched_at]
    validates_presence [:bid_id, :ask_id]
  end

end
