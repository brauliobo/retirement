class Sell < Trade

  def balance
    find_broker
  end

  def gain
    return unless buy
    price.growth buy.price
  end

end
