class LiveMessage < Sequel::Model

  many_to_one :balanced_pair

  unrestrict_primary_key

  def from_msg
    @from_msg ||= SymMash.new self[:from_msg]
  end

end

