ApplicationModel = Class.new Sequel::Model
class ApplicationModel

  plugin :validation_helpers

  def self.clear
    dataset.delete
  end

  def find_pair
    Pair.find broker: broker, name: pair
  end

  def find_broker
    Broker::ByName[broker]
  end

  dataset_module do

    def broker broker
      where broker: broker
    end

    def pair pair
      return where pair: pair unless pair.is_a? Pair
      self
        .broker(pair.broker)
        .where pair: pair.name
    end

  end

end
