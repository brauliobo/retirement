class User < Sequel::Model

  one_to_many :balanced_pairs

  def client broker = Broker::ByName.def
    tokens = SymMash.new self.tokens[broker.name]
    broker.client_for tokens[:key], tokens.secret
  end

  def buy p, quote_volume:, broker: :def
    broker = Broker::ByName[broker]
    resp   = SymMash.new broker.buy p, quote_volume: quote_volume, client: client(broker)
    raise resp.inspect if resp.code
    resp
  end

  def assets broker = Broker::ByName.def
    @assets ||= broker.assets client: client(broker)
  end

end
