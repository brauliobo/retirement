class PairPrice < ApplicationModel

  attr_accessor :signal_id
  attr_accessor :growth

  dataset_module do

    def interval interval = Pair.prices_fetch_days
      where interval: interval
    end

    def days days
      where{ time >= days.days.ago.beginning_of_hour }
    end

    def hours hours
      where{ time >= hours.hours.ago.beginning_of_hour }
    end

    def volume days: Pair.prices_fetch_days
      self
        .days(days)
        .sum(:volume)
    end

    def last_24h
      days 1
    end

    def last_48h
      days 2
    end

    def interval_15
      where Sequel.function(:to_char, :time, 'MI') => %w[00 15 30]
    end

  end

  protected

  def validate
    super
    validates_presence [:period]
  end

end
