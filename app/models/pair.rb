class Pair < ApplicationModel

  def self.find_or_create broker:, name:
    params = {broker: broker, name: name&.to_s}
    Pair.dataset.insert_conflict.insert params
    Pair.find params
  end

  def self.default
    find_or_create(
      broker: ENV['BROKER'],
      name:   ENV['PAIR'],
    )
  end

  class_attribute :prices_interval
  self.prices_interval = ENV['PRICES_INTERVAL'].to_i

  class_attribute :prices_fetch_days
  self.prices_fetch_days = ENV['PRICES_FETCH_DAYS'].to_i

  class_attribute :prices_long_interval
  self.prices_long_interval = ENV['PRICES_LONG_INTERVAL'].to_i

  class_attribute :prices_long_fetch_days
  self.prices_long_fetch_days = ENV['PRICES_LONG_FETCH_DAYS'].to_i


  class_attribute :prices_analysis_hours
  self.prices_analysis_hours = ENV['PRICES_ANALYSIS_HOURS'].to_i

  class_attribute :prices_long_analysis_hours
  self.prices_long_analysis_hours = ENV['PRICES_LONG_ANALYSIS_DAYS'].to_i * 24


  class_attribute :prices_analysis_method
  self.prices_analysis_method = Analyser::Prices.methods.first

  class_attribute :orders_analysis_method
  self.orders_analysis_method = Analyser::Orders.methods.first

  class_attribute :trade_on_analysers
  self.trade_on_analysers = ENV['TRADE_ON_ANALYSERS'].split.map(&:to_sym)

  class_attribute :trade_simulated
  self.trade_simulated = ENV['TRADE_SIMULATED'].in? %w[true 1]
  class_attribute :trade_real
  self.trade_real = ENV['TRADE_REAL'].in?(%w[true 1]) && !trade_simulated

  class_attribute :trade_min_gain
  self.trade_min_gain = ENV['TRADE_MIN_GAIN'].to_f

  dataset_module do

    def active
      where active: true
    end

  end

  alias_method :stats, :statistics

  def broker_module
    broker.camelize.constantize
  end
  def broker_instance
    Broker::ByName[broker.to_sym]
  end
  def broker_data
    SymMash.new self[:broker_data]
  end

  def accepted?
    broker_instance.pair_accepted? name
  end

  def base
    return name.split('_').first if name.index '_'
    broker_data.baseAsset
  end
  def quote
    return name.split('_').last  if name.index '_'
    broker_data.quoteAsset
  end

  def ticker
    broker_instance.ticker_24h self
  end

  def prices interval: prices_interval
    PairPrice
      .interval(interval)
      .order(Sequel.asc :time)
      .pair self
  end

  def prices_for_analysis hours: prices_analysis_hours, interval: prices_interval, sync: false
    Syncer::Prices.new.sync_interval pair: self, interval: interval, default_start: self.prices_fetch_days.days.ago if sync

    prices(interval: interval).hours hours
  end

  def analyse_prices method: prices_analysis_method, prices: nil, days: nil, **params
    generic_analyse name: method, prices: prices, days: days, **params
  end

  def analyse_orders method: orders_analysis_method, **params
    generic_analyse name: method, **params
  end

  def analyse_volatility **params
    generic_analyse name: :prices_volatility, **params
  end

  def analyse_fibonacci **params
    generic_analyse name: :fibonacci_retracement, **params
  end

  def analyse_volume_change **params
    generic_analyse name: :volume_change, **params
  end

  def analyse_price_pump **params
    generic_analyse name: :price_pump, **params
  end

  def generic_analyse name:, **params
    klass    = Analyser.const_get name.to_s.camelize
    analyser = klass.new(
      pair: self,
      **params
    )
    analyser.analyse
  end

  def orders
    PairOrder.pair self
  end

  def asks
    orders.asks
  end

  def bids
    orders.bids
  end

  def first_bid
    bids.first
  end

  def first_ask
    asks.first
  end

  def trades
    Trade.pair self
  end

  def buys
    trades.buys
  end

  def sells
    trades.sells
  end

  def analyser_params
    @analyser_params ||= SymMash.new self[:analyser_params]
  end

  def price_for_amount amount:, method: :asks
    find_broker.price_for_amount(
      pair:   self,
      amount: amount,
      method: :asks,
    )
  end

  def buy_from_signal analysis:
    return unless analysis.analyser.name.in? trade_on_analysers
    return unless analysis.analyser.min_gains?
    buy = buys.executed.unsold.where(analyser: analysis.analyser.name.to_s).first
    return if buy

    Buy.create(
      broker:     broker,
      pair:       name,
      pair_price: analysis.price,
      analyser:   analysis.analyser.name.to_s,
      pending:    true,
      simulated:  !trade_real,
    )
  end

  def sell_from_signal analysis:
    return unless analysis.analyser.name.in? trade_on_analysers
    buy = buys.executed.unsold.where(analyser: analysis.analyser.name.to_s).first
    return unless buy
    return unless analysis.price.close.growth(buy.price) > trade_min_gain

    sell = Sell.create(
      broker:     broker,
      pair:       name,
      pair_price: analysis.price,
      buy:        buy,
      amount:     buy.amount,
      analyser:   analysis.analyser.name.to_s,
      pending:    true,
      simulated:  buy.simulated,
    )
    buy.update sell_id: sell.id
    sell
  end

  protected

  def validate
    super
    validates_presence [:broker, :name]
  end

end
