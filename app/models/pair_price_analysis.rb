class PairPriceAnalysis < ApplicationModel

  many_to_one :price, class: PairPrice

  delegate_missing_to :price

  def as_json options = {}
    super.merge price: price.as_json
  end

  protected

  def before_validation
    self.broker = price.broker
    self.pair   = price.pair
    super
  end

  def validate
    super
    validates_presence [:broker, :pair]
    validates_presence [:price_id]
  end

end
