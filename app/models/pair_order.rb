class PairOrder < ApplicationModel

  plugin :single_table_inheritance, :type

  dataset_module do

    def last_fetched_at
      self
        .order(Sequel.desc :fetched_at)
        .limit(1)
        .select_map(:fetched_at)
        .first
    end

    def last_orders
      where fetched_at: self.last_fetched_at
    end

    def asks
      last_orders
        .where(type: 'PairAsk')
        .order(Sequel.asc :price)
    end
    alias_method :sells, :asks

    def bids
      last_orders
        .where(type: 'PairBid')
        .order(Sequel.desc :price)
    end
    alias_method :buys, :bids

    def volume
      sum(&:base_amount)
    end

  end

end
