class Trade < ApplicationModel

  plugin :single_table_inheritance, :type

  many_to_one :pair_price, key: :price_id

  many_to_one :buy
  many_to_one :sell

  def validate
    super
    validates_presence [:broker, :pair]
  end

  dataset_module do

    def buys
      where type: 'Buy'
    end

    def sells
      where type: 'Sell'
    end

    def pending
      where pending: true
    end
    def executed
      where pending: false
    end

    def simulated
      where simulated: true
    end

    def with_buy
      exclude buy_id: nil
    end

    def sold
      exclude sell_id: nil
    end
    def unsold
      where sell_id: nil
    end

  end

end
