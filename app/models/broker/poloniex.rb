module Broker
  class Poloniex < ::Broker::Base

    self.buy_sell_fee        = ENV['POLONIEX_FEE'].to_f

    # volumn in BTC
    self.minimum_pair_volume = ENV['MINIMUM_PAIR_VOLUME'].to_f

    self.trading_api_enabled = ENV['POLONIEX_KEY'].present?

    def available
      ::Poloniex::Balance.fetch[base_coin.to_s]
    rescue
      available_in_base_pair
    end

    def pair_accepted? pair, obj
      return false unless pair.to_s.start_with? base_coin.to_s
      has_minimum = (obj.is_a?(Hash) and obj[base_coin].to_f >= self.minimum_pair_volume)
      puts "Volume of pair #{pair} is #{obj[base_coin].to_f}. Skipping.." if !has_minimum and ENV['DEBUG']
      has_minimum
    end

    def self.deactivate_delisted coins
      pairs = coins.split(',').map(&:squish).map{ |c| "BTC_#{c}" }
      Pair.where(broker: self.new.name).where(name: pairs).update active: false
    end

    def pairs
      # caching is causing problems
      #return @pairs if @pairs

      @pairs = ::Poloniex::Volume.fetch
      @pairs.select!{ |p, o| pair_accepted? p,o }
      #@pairs.select!{ |p, o| o.is_a? Hash and o.values.first.to_f > 0 }

      @pairs = @pairs.keys
      @pairs.map!{ |p| Pair.find_or_create broker: self.name, name: p }
      @pairs.select!(&:active)
      @pairs.sort_by!(&:name)
      @pairs
    end
    synchronize :pairs

    def balances
      res = ::Poloniex::Balance.fetch
      res.each do |pair, data|
        trades = ::Poloniex::History.fetch pair: pair
        totals = trades.first.second.map(&:total).map(&:to_f)

        data.buyValue = totals.sum
        data.buyPrice = data.buyValue / data.total
      end
      res
    end

    def price_for_amount pair:, amount:, method: :asks
      orders = ::Poloniex::OrderBook.fetch pair: pair
      orders = orders.to_orders method: method

      order = orders.find{ |x| x.base_sum > amount }
      order&.price
    end

    def buy pending_buy
      pair    = pending_buy.find_pair
      amount  = pending_buy.amount || buy_base_amount
      price   = pending_buy.price
      price ||= price_for_amount pair: pair, amount: amount, method: :asks

      pending_buy.update(
        price:  price,
        amount: amount,
      )

      execute_trade pending_buy
    end

    def sell pending_sell
      pair    = pending_sell.find_pair
      amount  = pending_sell.amount
      price   = pending_sell.price
      price ||= price_for_amount pair: pair, amount: amount, method: :bids

      if  pending_sell.simulated
        balance = balances[pair.name]
        return pending_sell.destroy unless balance
        raise "#{name}/#{pair.name}: No available balance to be sold" if balance.zero?

        amount  = balance.available if amount.blank?
      end

      pending_sell.update(
        price:  price,
        amount: amount,
      )

      execute_trade pending_sell
    end

    def execute_trade pending_trade
      pair   = pending_trade.find_pair

      if pending_trade.simulated
        pending_trade.update(
          pending:     false,
          executed_at: Time.now,
        )
      else
        type = if pending_trade.is_a? Buy then :buy else :sell end
        res  = ::Poloniex::Trade.send(type,
          pair:   pair.name,
          price:  pending_trade.price,
          volume: pending_trade.amount,
        )

        if res.resultingTrades.present? and res.amountUnfilled.to_f.zero?
          pending_trade.update(
            pending:     false,
            executed_at: Time.now,
          )
        end
      end

      pending_trade
    end

    def pair_prices p, int, periods
      ChartData.fetch pair: p, interval: int, start_time: start_time
    end

    def sync_orders pair:
      Syncer::Orders.new.sync pair: pair
    end

    def ticker_subscribe receive:
      wamp = ::Poloniex::Wamp.new receive_ticker: receive
      Thread.new{ wamp.start }
    end

  end
end
