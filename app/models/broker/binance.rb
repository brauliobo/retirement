module Broker
  class Binance < ::Broker::Base

    class_attribute :public_client
    self.public_client = ::Binance::Client::REST.new

    def self.exchange_info
      SymMash.new public_client.exchange_info
    end

    def client_for key, secret
      ::Binance::Client::REST.new api_key: key, secret_key: secret
    end

    def pairs update: false
      @pairs = self.class.exchange_info
      @pairs = @pairs.symbols
      @pairs.select!{ |p| p.status == 'TRADING' and p.isSpotTradingAllowed }
      DB.transaction do
        @pairs.map! do |_p|
          p = Pair.find_or_create broker: self.name, name: _p.symbol
          p.update broker_data: _p
          p
        end
      end
      @pairs.api_peach do |p|
        next p if p.volume_24h.present? and 24.hours.ago < p.updated_at

        puts "#{name}/#{p.name}: fetching ticker info"
        ticker_24h = SymMash.new public_client.twenty_four_hour symbol: p.name
        p.update volume_24h: ticker_24h.volume.to_f
        p
      end if update

      blah = @pairs
      blah
    end

    def buy p, quote_volume:, price: nil, type: 'MARKET', client:
      params = SymMash.new(
        symbol:        p.name,
        side:          'BUY',
        type:          type,
        quoteOrderQty: quote_volume,
      )
      if type != 'MARKET'
        params.price = price
        params.time_in_force = 'GTC'
      end

      client.create_order! params
    end

    def ticker_24h pair
      SymMash.new public_client.twenty_four_hour symbol: pair.name
    end

    def assets client:
      resp   = SymMash.new client.account_info
      resp.balances
        .select{ |a| a.free.to_f > 0 }
        .each.with_object({}){ |a, h| h[a.asset] = a }
        .transform_values!{ |a| SymMash.new available: a.free.to_f }
    end

    def pair_prices p, int, periods
      data = public_client.klines symbol: p.name, interval: int.downcase, limit: periods
      data.map! do |cd|
        SymMash.new(
          date_time:  Time.at(cd[0]/1000),
          close_time: Time.at(cd[6]/1000),
          open:       cd[1].to_f,
          high:       cd[2].to_f,
          low:        cd[3].to_f,
          close:      cd[4].to_f,
          volume:     cd[5].to_f,
          trades:     cd[7].to_f,
        )
      end
    end

  end
end

