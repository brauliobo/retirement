module Broker
  class Base

    class_attribute :base_coin
    self.base_coin = ENV['BASE_COIN'].to_sym

    class_attribute :available_in_base_pair
    self.available_in_base_pair = ENV['AVAILABLE_IN_BASE_PAIR'].to_f

    class_attribute :short_buy_base_amount
    self.short_buy_base_amount = ENV['SHORT_BUY_BASE_AMOUNT'].to_f

    class_attribute :buy_base_amount
    self.buy_base_amount = self.short_buy_base_amount

    class_attribute :trading_api_enabled
    self.trading_api_enabled = false

    class_attribute :buy_sell_fee
    self.buy_sell_fee = 0

    class_attribute :minimum_pair_volume
    self.minimum_pair_volume = 0

    def name
      @name ||= self.class.name.demodulize.underscore
    end

    def available
      raise 'not implemented'
    end

    def pair_accepted? pair
      true
    end

    def pairs
      Pair.broker(name)
    end

    def balances
      raise 'not implemented'
    end

    def sell_at pair:, price:
      Sell.create pair: pair.name, pending: true, price: price
    end

    def price_for_amount pair:, amount:, method: :asks
      raise 'not implemented'
    end

    def sell pending_sell
      raise 'not implemented'
    end

    def buy pending_buy
      raise 'not implemented'
    end

    def pending_sells
      Sell.pending.broker(name)
    end

    def pending_buys
      Buy.pending.broker(name)
    end

    def execute_trade pending_trade
      raise 'not implemented'
    end

    def sync_orders pair:
      raise 'not implemented'
    end

    def ticker_subscribe receive:
      raise 'not implemented'
    end

  end
end
