module Broker

  ByName = SymMash.new(
    def:      Broker::Binance.new,
    binance:  Broker::Binance.new,
    poloniex: Broker::Poloniex.new,
  )

  mattr_accessor :all
  self.all = ByName.values

  mattr_accessor :first
  self.first = all.first

  mattr_accessor :poloniex
  self.poloniex = ByName.poloniex

end
