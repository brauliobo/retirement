class BalancedPair < ApplicationModel

  many_to_one :user
  many_to_one :pair

  def current_quote_volume price: pair.ticker.lastPrice.to_f
    base_quote_volume * price
  end

end
