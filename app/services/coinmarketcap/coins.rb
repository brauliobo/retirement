module Coinmarketcap
  class Coins

    Url = 'https://api.coinmarketcap.com/v1/ticker/'

    def self.fetch
      response = HttpClient.new.get Url
      coins    = JSON.parse response.body
      coins    = coins.map{ |c| SymMash.new c }

      coins.map do |coin|
        SymMash.new(
          symbol:           coin.symbol,
          name:             coin.name,
          coinmarketcap_id: coin.id,
        )
      end
    end

  end
end
