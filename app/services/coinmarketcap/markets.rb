module Coinmarketcap
  class Markets

    Url = 'https://coinmarketcap.com/currencies/%{id}'

    def self.fetch id:
      url  = Url % {id: id}
      page = Mechanize.new.get url

      data = page.css('#markets-table tbody tr').map do |market|
        cols   = market.css(:td)
        broker = cols[1].at(:a).attr('href').split('/').last
        pair   = cols[2].text.gsub '/', '_'
        volume = cols[3].at(:span).attr('data-btc').to_f
        price  = cols[4].at(:span).attr('data-btc').to_f

        next unless pair.index Broker::Base.base_coin.to_s

        SymMash.new(
          broker:   broker,
          pair:     pair,
          volume:   volume,
          price:    price,
          interval: 24*60,
        )
      end.compact

      data.sort_by!(&:price)
      data
    end

  end
end

