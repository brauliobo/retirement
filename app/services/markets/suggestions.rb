module Markets
  class Suggestions

    class_attribute :base_broker
    self.base_broker = ENV['BASE_BROKER']

    class_attribute :min_volume
    self.min_volume = ENV['MARKET_MIN_VOLUME'].to_i

    class_attribute :min_growth
    self.min_growth = ENV['MARKET_MIN_GROWTH'].to_i

    def self.max_market_variation
      last_prices = PairPrice
        .select(:broker, :pair)
        .select_append{ max(:time).as :time }
        .group(:pair, :broker)
        .where{ time >= 1.day.ago }

      pairs = PairPrice
        .join(last_prices, broker: :broker, pair: :pair, time: :time)
        .all.group_by(&:pair)

      prices = pairs.each.with_object([]) do |(pair, prices), r|
        base_price = prices.find{ |p| p.broker == base_broker }
        next unless base_price
        prices.delete base_price

        prices.select! do |p|
          next unless p.volume >= min_volume

          p.growth = p.close.growth base_price.close
          next unless p.growth.abs >= min_growth

          true
        end
        next if prices.empty?

        prices.sort_by!{ |p| p.growth.abs }.reverse!

        r << [base_price, prices]
      end

      prices.sort_by! do |base, prices|
        prices.first.growth.abs
      end.reverse!

      prices.map do |base, prices|
        r = {
          coin:          base.pair,
          base_broker => base.close,
        }

        prices.first(3).each.with_index do |p,i|
          r["price_#{i}"] = "#{p.broker}: #{p.close} #{p.growth}%"
        end

        r
      end
    end

  end
end
