module Poloniex
  class Trade < SymMash

    extend TradingApi

    Types = %i[
      fill_or_kill
      immediate_or_cancel
      post_only
    ]
    DefaultType = :immediate_or_cancel

    def self.buy pair:, price:, volume:, type: DefaultType
      trade command: 'buy',  pair: pair, price: price, volume: volume, type: type
    end

    def self.sell pair:, price:, volume:, type: DefaultType
      trade command: 'sell', pair: pair, price: price, volume: volume, type: type
    end

    protected

    def self.trade command:, pair:, price:, volume:, type:
      type = type.to_s.camelize :lower
      params = {
        currencyPair: pair,
        rate:         price,
        amount:       volume,
        type =>       1,
      }
      data   = trading_post command: command, params: params

      new data
    end

  end
end
