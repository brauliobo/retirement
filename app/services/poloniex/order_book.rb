module Poloniex
  class OrderBook < SymMash

    extend Api

    Url = 'https://poloniex.com/public?command=returnOrderBook&currencyPair=%{pair}&depth=%{depth}'

    class_attribute :orders_fetch_depth
    self.orders_fetch_depth = ENV['ORDERS_FETCH_DEPTH'].to_i

    def self.fetch pair: Pair.default, depth: orders_fetch_depth
      params = {
        pair:  pair.name,
        depth: depth,
      }
      book   = get Url % params

      new book.merge pair: pair, time: Time.now
    end

    def to_asks
      to_orders method: :asks
    end
    def to_bids
      to_orders method: :bids
    end

    def to_orders method:
      klass = "Pair#{method.to_s.singularize.camelize}"

      base_sum    = 0
      counter_sum = 0

      send(method).map do |order|
        price          = order.first.to_f
        counter_amount = order.second.to_f
        counter_sum   += counter_amount
        base_amount    = price * counter_amount
        base_sum      += base_amount

        SymMash.new(
          type:           klass,
          fetched_at:     time,
          broker:         pair.broker,
          pair:           pair.name,
          price:          price,
          base_amount:    base_amount,
          counter_amount: counter_amount,
          base_sum:       base_sum,
          counter_sum:    counter_sum,
        )
      end
    end

  end
end
