module Poloniex
  class ChartData < SymMash

    extend Api

    URL       = 'https://poloniex.com/public?command=returnChartData&currencyPair=%{pair}&start=%{start}&end=%{end}&period=%{period}'
    INTERVALS = SymMash.new(
      '5m':    5,
      '15m':   15,
      '30m':   30,
      '120m':  2*60,
      '240m':  4*60,
      '1440m': 24*60,
      '2h':    2*60,
      '4m':    4*60,
      '1d':    24*60,
    )

    def self.fetch pair:, interval: INTERVALS[pair.prices_interval], start_time: pair.prices_fetch_days.days.ago.beginning_of_hour, end_time: Time.now

      url = URL % {
        pair:     pair.name,
        interval: interval*60,
        start:    start_time.to_i,
        end:      end_time.to_i,
      }
      prices = get url

      new(
        pair:     pair,
        start:    start_time,
        interval: interval,
        prices:   prices,
      )

    rescue => e
      retry if e.class == JSON::ParserError
      raise
    end

    def to_pair_prices
      prices.map do |price|
        SymMash.new(
          broker:   pair.broker,
          pair:     pair.name,
          time:     Time.at(price.date),
          interval: interval,

          open:     price.open,
          high:     price.high,
          low:      price.low,
          close:    price.close,

          volume:   price.volume,
        )
      end
    end

  end
end
