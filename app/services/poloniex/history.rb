module Poloniex
  class History < SymMash

    extend TradingApi

    class_attribute :months
    self.months = ENV['POLONIEX_HISTORY_MONTHS'].to_i

    def self.fetch pair: nil, from_balances: true
      params = {
        currencyPair: pair || 'all',
        start:        months.months.ago.to_i,
      }
      data   = trading_post command: 'returnTradeHistory', params: params

      if pair
        data = new pair => data
      else
        data = new data
      end

      if from_balances
        balances = Balance.fetch

        data.each do |p, trades|
          next unless b = balances[p]
          amount = 0
          data[p] = trades.each.with_object [] do |t, a|
            next unless t.type == 'buy'
            break a if amount > b.total
            amount += t.amount.to_f
            a << t
          end
        end
      end

      data
    end

  end
end
