module Poloniex
  class OpenOrders < SymMash

    extend TradingApi

    def self.fetch pair: :all
      params = {
        currencyPair: pair,
      }
      data   = trading_post command: 'returnOpenOrders', params: params

      d = new data
      d.select!{ |p, orders| orders.present? }
      d
    end

  end
end
