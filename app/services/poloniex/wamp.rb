module Poloniex
  class Wamp

    attr_reader :receive_ticker

    ##
    # Example:
    #   Poloniex::Wamp.new(receive_ticker: -> data { pp data }).start
    #
    def initialize receive_ticker:
      @receive_ticker = receive_ticker
    end

    def start
      connection = WampClient::Connection.new(
        uri:   'wss://api.poloniex.com',
        realm: 'realm1',
      )

      connection.on_connect do
        puts 'poloniex/wamp: connect'
      end
      connection.on_leave do |session, details|
        puts 'poloniex/wamp: leave'
      end
      connection.on_disconnect do |reason|
        puts 'poloniex/wamp: disconnect'
      end

      connection.on_join do |session, details|
        puts 'poloniex/wamp: open'

        session.subscribe 'ticker', method(:ticker)
      end

      connection.open
    end

    def ticker args, kwargs, details
      fields = %i[pair last_price lowest_ask highest_bid percert_change base_volume quote_volume is_frozen 24hr_high]
      params = SymMash.new Hash[fields.zip args]
      pair   = params.delete :pair
      params.each{ |k,v| params[k] = v.to_f if k != :pair }
      params.pair = Pair.find_or_create broker: 'poloniex', name: pair

      receive_ticker.call params
    end

  end
end
