module Poloniex
  class Currencies

    extend Api

    Url = 'https://poloniex.com/public?command=returnCurrencies'

    def self.fetch
      SymMash.new get Url
    end

  end
end

