module Poloniex
  class Volume < SymMash

    extend Api

    Url = 'https://poloniex.com/public?command=return24hVolume'

    def self.fetch
      new get Url
    end

  end
end

