module Poloniex
  class Balance < SymMash

    extend TradingApi

    def self.fetch base_coin: Broker::Poloniex.base_coin
      params = {
      }
      data   = trading_post command: 'returnCompleteBalances', params: params

      d = new data
      d.each{ |c,h| h.each{ |k,v| h[k] = v.to_f } }

      d.select!{ |c,h| h.btcValue > 0 }

      if base_coin
        d.reject!{ |c,h| c == base_coin }
        d.keys.each{ |c| d["#{base_coin}_#{c}"] = d.delete c }
      end

      d.each{ |c,h| h.total = h.available + h.onOrders }

      d
    end

  end
end
