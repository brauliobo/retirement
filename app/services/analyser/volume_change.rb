module Analyser
  class VolumeChange < PricesBase

    class_attribute :signal_percentage
    self.signal_percentage = ENV['VOLUME_CHANGE_SIGNAL_PERCENTAGE'].to_f

    def initialize pair:
      super(
        pair:  pair,
        cache: false,
      )
    end

    def analyse
      prices        = @prices.all
      last_but_one  = prices[-2]
      last          = prices[-1]

      period_change = last.volume.growth(last_but_one.volume)
      price_change  = last.close.growth(last_but_one.close)

      has_signal    = period_change >= signal_percentage
      signal        = if price_change.positive? then 'up' else 'down' end if has_signal

      analyses = [
        SymMash.new(
          period_change: period_change,
          price_change:  price_change,
          signal:        signal,
        ),
      ]
      @result = Analysis::BaseResult.new analyses, analyser: self
      @result.signal = @result.first.signal
      @result
    end

    def report
      <<-EOS
\tvolume change: #{@result.first.period_change}%
\tprice change:  #{@result.first.price_change}%
      EOS
    end

  end
end
