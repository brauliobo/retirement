module Analyser
  class Orders < OrdersBase

    class_attribute :methods
    self.methods = ENV['ORDERS_ANALYSERS'].split.map(&:to_sym)

    def analyse
      @result = Analysis::MultiResult.new methods.each.with_object({}) do |method, h|
        h[method] = pair.analyse_orders method: method
      end
    end

    def print_report
      @result.each do |method, r|
        r.analyser.print_report
      end
    end

  end
end
