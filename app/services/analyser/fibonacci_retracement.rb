module Analyser
  class FibonacciRetracement < AveragesBase

    self.modes = %i[medium]

    class_attribute :price_method
    self.price_method = :medium_avg

    class_attribute :calib_hours
    self.calib_hours = ENV['FIBONACCI_CALIB_HOURS'].to_i

    class_attribute :bars
    self.bars = ENV['FIBONACCI_BARS'].to_i

    class_attribute :iterations
    self.iterations = ENV['FIBONACCI_ITERATIONS'].to_i

    def initialize pair:, **params
      super(
        pair:   pair,
        cache:  false,
        hours:  params[:hours] || calib_hours,
        **params
      )
    end

    def analyse
      init_analyses do |a|
        a.level = nil
      end
      calculate_avg_growth

      calib_start = calib_hours.hours.ago

      last      = analyses.last
      calib_min = last
      calib_max = last

      analyses.reverse_each do |a|
        break if a.time < calib_start
        calib_min = a if a[price_method] < calib_min[price_method]
        calib_max = a if a[price_method] > calib_max[price_method]
      end
      calib_min.level = 'calib_min'
      calib_max.level = 'calib_max'

      # ensure minimum volatility
      calib_growth = calib_max[price_method].growth calib_min[price_method]
      return if calib_growth.abs < 10

      if calib_max.time > calib_min.time
        # ascending
        analyses.reverse_each do |a|
        end
      else
        # descending
        min = analyses.last
        max = analyses.last
        analyses.reverse_each do |a|
          min = a if a[price_method] < min[price_method]
          max = a if a[price_method] > max[price_method]

          a.level ||= a[price_method].level_in max[price_method], min[price_method]
        end
        puts min
        puts max
      end

      puts "calib_max:    #{point_msg calib_max}"
      puts "calib_min:    #{point_msg calib_min}"
      puts "calib_growth: #{calib_growth}%"

      @result = Analysis::BaseResult.new analyses, analyser: self
    end

    def point_msg a
      "#{a.time}: #{a[price_method]}"
    end

    def print_report rows: 20
      tp @result.last(rows), except: [:price, :close]
      puts "Local gains: #{@result.gains}%"
    end

  end
end
