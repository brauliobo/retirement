module Analyser
  class AveragesAlignment < AveragesBase

    self.modes = %i[short medium long]

    self.buy_length = 5

    def analyse
      broker    = pair.find_broker
      buy_price = nil
      amount    = 1
      losses    = 0

      calculate_avg_growth

      analyses.each.with_index do |a, i|
        a.signals = []

        ip = if i == 0 then 0 else i - 1 end
        ap = analyses[ip]

        buy_signal    = analyses.rewind_range(i-1, buy_length).all? do |ar|
          ar.short_avg < ar.medium_avg
        end
        buy_signal  &&= a.short_avg > a.medium_avg && a.medium_avg < a.long_avg && a.medium_avg > -0.1
        buy_signal  &&= a.medium_growth > -0.1

        sell_signal   = true
        sell_signal &&= analyses.rewind_range(i, sell_length).all? do |ar|
          ar.short_growth < 0
        end

        # rapid growth can happen in down trends too
        a.signals << 'rapid growth' if a.short_growth > rapid_growth

        if buy_signal
          a.signal  = 'up buy'
          buy_price = a.close
        elsif sell_signal
          gain      = a.close.growth(buy_price) - broker.buy_sell_fee
          a.signal  = "down sell #{gain.round 2}%"
          amount   += amount * gain/100
          losses   += gain if gain < 0
        elsif a.short_avg > a.medium_avg
          a.signal  = 'up'
        else
          a.signal  = 'down'
        end
      end

      @result = Analysis::BaseResult.new analyses, analyser: self
      @result.price       = analyses.last.price
      @result.buy_signal  = 'up buy'    if analyses.last.signal.index 'up buy'
      @result.sell_signal = 'down sell' if analyses.last.signal.index 'down sell'
      @result.signal      = @result.buy_signal || @result.sell_signal
      @result.signals     = analyses.last.signals
      @result.gains       = amount.growth(1).round 2
      @result.losses      = losses

      @long_term.analyse if @long_term

      @result
    end

    def report
      signal = @result.signal.upcase
      msg    = "Averages signaling #{signal}"

      signals = @result.signals - [@result.signal]
      signals = signals.map(&:upcase).join ' '
      msg    += ", also #{signals}" if signals.present?

      msg
    end

    def print_report rows: 20
      tp @result.last(rows), except: [:price]
    end

  end
end

