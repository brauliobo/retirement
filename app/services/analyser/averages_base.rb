module Analyser
  class AveragesBase < PricesBase

    class_attribute :modes
    self.modes = %i[short]

    class_attribute :avg_method
    self.avg_method = ENV['AVERAGES_METHOD']

    class_attribute :rapid_growth
    self.rapid_growth = ENV['AVERAGES_RAPID_GROWTH'].to_f

    class_attribute :growth_length
    self.growth_length = ENV['AVERAGES_GROWTH_LENGTH'].to_i

    class_attribute :length_mult
    self.length_mult = ENV['AVERAGES_LENGTH_MULT'].to_i

    class_attribute :short_length
    self.short_length = ENV['AVERAGES_SHORT_LENGTH'].to_i * length_mult

    class_attribute :medium_length
    self.medium_length = ENV['AVERAGES_MEDIUM_LENGTH'].to_i * length_mult

    class_attribute :long_length
    self.long_length = ENV['AVERAGES_LONG_LENGTH'].to_i * length_mult

    class_attribute :min_gains
    self.min_gains = ENV['AVERAGES_MIN_GAINS'].to_f

    class_attribute :buy_length
    self.buy_length = 2
    class_attribute :sell_length
    self.sell_length = 2

    class_attribute :buy_thold
    self.buy_thold = 0
    class_attribute :sell_thold
    self.sell_thold = 0

    attr_reader :long_term

    def initialize pair:, skip_long: true, **params
      super(
        pair:   pair,
        cache:  false,
        **params
      )

      init_analyses

      @long_term = Averages.new(
        pair:     pair,
        interval: Pair.prices_long_interval,
        hours:    Pair.prices_long_analysis_hours,
      ) unless skip_long or params[:interval]
    end

    def calculate_avg_growth
      modes.each do |mode|
        length = send "#{mode}_length"

        analyses.each.with_index do |a, i|
          avg = analyses.rewind_range(i, length).map(&:close).send avg_method
          a["#{mode}_avg"] = avg.signif

          initial_avg = analyses.rewind(i, growth_length)["#{mode}_avg"]
          a["#{mode}_growth"] = avg.growth initial_avg
        end
      end
    end

  end
end
