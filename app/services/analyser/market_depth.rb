module Analyser
  class MarketDepth < OrdersBase

    class_attribute :trend_min
    self.trend_min = ENV['MARKET_DEPTH_TREND_MIN'].to_i

    class_attribute :max_order_position
    self.max_order_position = ENV['MARKET_DEPTH_MAX_ORDER_POSITION'].to_i

    def initialize pair: Pair.default
      params = {trend_min: trend_min, max_order_position: max_order_position}

      super(
        pair:   pair,
        params: params,
      )
    end

    def analyse trend_min: self.trend_min
      asks_growth   = orders_growth @asks
      bids_growth   = orders_growth @bids
      zipped_growth = asks_growth.zip(bids_growth).take_while{ |a,b| a && b }

      # build ratios
      analyses = zipped_growth.map do |ask, bid|
        analysis = PairOrderAnalysis.new params: @params, data: {}

        analysis.ask = ask.delete :order
        analysis.bid = bid.delete :order
        ask.each{ |k,v| analysis[:"ask_#{k}"] = ask[k] }
        bid.each{ |k,v| analysis[:"bid_#{k}"] = bid[k] }

        analysis.bid_ask_ratio = bid.growth.growth(ask.growth)
        analysis
      end
      analyses = Analysis::OrdersResult.new analyses, analyser: self

      # build trends
      analyses.each.with_index do |a, i|
        next a.values.merge! trend: nil, signal: nil, data: {derivative: 0} if i == 0

        a.trend  = if a.bid_ask_ratio.positive? then 'up' else 'down' end
        a.signal = nil

        ap = analyses[i-1]
        a.data[:derivative] = ap.bid_ask_ratio.growth(a.bid_ask_ratio)
      end

      # select signals
      first_trend    = analyses[1].trend

      st_signal = analyses[1..trend_min].all?{ |a| a.trend == first_trend }
      lt_signal = analyses.last.trend == first_trend

      if st_signal and lt_signal
        method   = if first_trend == 'up' then :max_by else :min_by end
        analysis = analyses[1..max_order_position].send(method){ |a| a.bid_ask_ratio }
        analysis.signal = first_trend
        analyses.signal = analysis.signal
      end

      @result = analyses

      persist if @result.signal

      @result
    end

    def orders_growth orders
      v = 0
      orders.map do |o|
        SymMash.new(
          order:  o,
          volume: (v += o.base_amount).round(3),
          growth: (v / o.price).round(3),
        )
      end
    end

    def print_report
      tp result.first max_order_position
    end

  end
end
