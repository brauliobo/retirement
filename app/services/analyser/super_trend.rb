module Analyser
  class SuperTrend < PricesBase

    include Analysis::Utils
    include Analysis::Navigation
    include Analysis::Atr

    class_attribute :range
    self.range = ENV['SUPER_TREND_RANGE'].to_sym

    def initialize pair:, prices: nil, days: nil, range: self.range, factor: nil, pd: nil, **opts
      case range
      when :hours
        @factor,@pd = 12, 100
      when :short
        @factor,@pd = 2, 21
      when :very_short
        @factor,@pd = 1, 3
      when :default
        @factor,@pd = 3, 7
      else
        @factor,@pd = factor || 3, pd || 7
      end

      super(
        pair:   pair,
        prices: prices,
        params: {factor: @factor, pd: @pd},
        cache:  true,
        **opts
      )
    end

    def analyse
      return @result if @d.blank?

      @pp = @d[0]
      @pp.trend_up   ||= Integer::MIN
      @pp.trend_down ||= Integer::MAX

      @d.each.with_index do |p, i|
        @p = p
        @i = i

        @pp = @d[ip i]

        analyse_point
      end

      persist

      @result
    end

    protected

    def analyse_point
      atr l: @pd

      up   = hl2 - (@factor * @p.atr)
      down = hl2 + (@factor * @p.atr)

      @p.trend_up   = if @pp.close > @pp.trend_up   then [up,   @pp.trend_up].max   else up end
      @p.trend_down = if @pp.close < @pp.trend_down then [down, @pp.trend_down].min else down end

      @p.trend = if @p.close > @pp.trend_down then 1 elsif @p.close < @pp.trend_up then -1 else nz @pp.trend, 1 end
      @p.tsl   = if @p.trend == 1 then @p.trend_up else @p.trend_down end

      check_signal
    end

    def check_signal
      @p.signal = 'up'   if @p.trend ==  1 and @pp.trend == -1
      @p.signal = 'down' if @p.trend == -1 and @pp.trend ==  1
    end

  end
end
