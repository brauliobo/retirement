module Analyser
  class PricesBase < Base

    class_attribute :base_syncer
    self.base_syncer = :prices

    attr_reader :hours
    attr_reader :prices
    attr_reader :interval
    attr_reader :params

    attr_reader :analyses
    attr_reader :result

    alias_method :data, :prices

    def initialize pair:, prices: nil, hours: Pair.prices_analysis_hours, interval: Pair.prices_interval, sync: false, cache: false, params: {}, **others
      super pair: pair, **others

      @hours    ||= hours
      @prices   ||= prices
      @interval ||= interval
      @params   ||= params

      @prices ||= pair.prices_for_analysis(
        interval: @interval,
        hours:    @hours,
        sync:     sync,
      )

      if cache
        @result = cache_read prices: @prices, params: @params
        start   = if @result.start.zero? then 0 else @result.start-1 end
        @d      = @result[start..-1]
      end
    end

    def cache_read prices:, params:
      analyses  = PairPriceAnalysis
        .eager(:price)
        .pair(pair)
        .where(params: params.pg_jsonb)
        .where(price_id: prices.map(&:id))
        .order(:price_id)
        .all
      analyses  = Analysis::PricesResult.new analyses, analyser: self
      analyses.start = analyses.size

      price_ids = Set.new analyses.map(&:price_id)
      prices    = prices.reject{ |p| p.id.in? price_ids }
      prices.each do |p|
        next if p.new?
        analysis = PairPriceAnalysis.new price: p, params: params
        analyses << analysis
      end

      analyses
    end

    def persist
      DB.transaction{ @d.each{ |p| p.save if p.new? } }
    end

    def init_analyses
      @analyses = @prices.all.each.with_index.map do |p,i|
        a = SymMash.new(
          i:      i,
          time:   p.time,
          signal: nil,
          price:  p,
          close:  p.close,
        )
        yield a if block_given?
        a
      end
    end

  end
end
