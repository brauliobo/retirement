module Analyser
  class Base

    # to be changed in subclasses
    class_attribute :notification_timeout
    self.notification_timeout = Notifier.expire_time

    attr_reader :pair
    attr_reader :params

    attr_accessor :result

    def initialize pair: nil, **others
      @pair = pair || Pair.default
      params_set @pair.analyser_params[name]&.params
      params_set others
    end

    def name
      @name ||= self.class.name.demodulize.underscore.to_sym
    end

    def parse_signal signal: @result.signal
      "signal: #{signal.upcase}" if signal
    end

    def print_header method: self.name
      puts "analyser/#{method}: #{pair.broker}/#{pair.name}: #{parse_signal}"
    end

    def report
      raise 'not implemented'
    end

    def print_report method: self.name, &block
      puts report
    end

    def report?
      true
    end

    def notify?
      true
    end

    def skip?
      false
    end

    def notify
      return if method(:report).owner == Base

      Notifier.notify(
        summary:     "#{pair.broker}/#{pair.name}",
        body:        report,
        expire_time: notification_timeout,
      )
    end

  end
end
