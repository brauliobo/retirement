module Analyser
  class OrdersBase < Base

    class_attribute :base_syncer
    self.base_syncer = :orders

    def initialize pair: nil, params: {}
      super pair: pair

      @asks   = @pair.asks.all
      @bids   = @pair.bids.all
      @params = params
    end

    def data
      @pair.orders
    end

    def persist
      DB.transaction{ @result.each{ |p| p.save if p.new? } }
    end

  end
end
