module Analyser
  class LocalAverages < AveragesBase

    self.modes = %i[local medium]

    class_attribute :local_mode
    self.local_mode = :medium
    class_attribute :local_length
    self.local_length = self.send "#{local_mode}_length"

    def analyse
      broker    = pair.find_broker
      buy_price = nil
      amount    = 1
      losses    = 0

      calculate_avg_growth

      analyses.each.with_index do |a, i|
        ip = if i == 0 then 0 else i - 1 end
        ap = analyses[ip]

        #a.deviation = analyses.rewind_range(i, growth_length).map(&:close).standard_deviation.signif
        #a.variance  = analyses.rewind_range(i, growth_length).map(&:close).variance.signif

        growing   = analyses.rewind_range(i, buy_length).all? do |ar|
          ar.local_growth > buy_thold
        end

        if ap.signal.nil?
          a.signal = ap.signal = if growing then 'up' else 'down' end
        else
          buy_signal    = !buy_price
          buy_signal  &&= growing

          sell_signal   = buy_price
          sell_signal &&= analyses.rewind_range(i, sell_length).all? do |ar|
            ar.local_growth < sell_thold
          end
          sell_signal &&= a.local_growth <= analyses.rewind(i, sell_length).local_growth

          if buy_signal
            a.signal  = 'up buy'
            buy_price = a.close
          elsif sell_signal
            gain      = a.close.growth(buy_price) - broker.buy_sell_fee
            a.signal  = "down sell #{gain.round 2}%"
            buy_price = nil
            amount   += amount * gain/100
            losses   += gain if gain < 0
          else
            a.signal  = if growing then 'up' else a.signal = 'down' end
          end
        end
      end

      @result = Analysis::BaseResult.new analyses, analyser: self
      @result.price       = analyses.last.price
      @result.buy_signal  = 'up buy'    if analyses.last.signal.index 'up buy'
      @result.sell_signal = 'down sell' if analyses.last.signal.index 'down sell'
      @result.signal      = @result.buy_signal || @result.sell_signal
      @result.gains       = amount.growth(1).round 2
      @result.losses      = losses

      @result
    end

    def min_gains?
      @result.gains > min_gains
    end

    def report?
      min_gains?
    end
    def notify?
      min_gains?
    end

    def skip?
      pair.analyser_params&.send(name).blank?
    end

    def report
      signal = @result.signal.upcase
      signal = "<b>#{signal}</b>" if signal == 'UP'
      "Average signaling #{signal}"
    end

    def print_report rows: 20
      tp @result.last(rows), except: [:price]
      puts "Local gains: #{@result.gains}%"
    end

  end
end
