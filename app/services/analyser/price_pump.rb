module Analyser
  class PricePump < PricesBase

    class_attribute :interval
    self.interval = ENV['PRICE_PUMP_INTERVAL']

    class_attribute :bars
    self.bars = ENV['PRICE_PUMP_BARS'].to_i

    class_attribute :price_variation
    self.price_variation = ENV['PRICE_PUMP_PRICE_VARIATION'].to_f

    class_attribute :volume_variation
    self.volume_variation = ENV['PRICE_PUMP_VOLUME_VARIATION'].to_f

    class_attribute :max_non_trend_bars
    self.max_non_trend_bars = ENV['PRICE_PUMP_MAX_NON_TREND_BARS'].to_i

    def initialize pair: nil
      super(
        pair:     pair,
        interval: interval,
        days:     0,
        sync:     true,
        cache:    false,
      )

      @prices = @prices
        .order(Sequel.desc :time)
        .limit(bars)
    end

    def analyse
      prices     = @prices.all
      price_var  = prices.first.high.growth prices.last.close
      volume_var = prices.first.volume.growth prices.last.volume

      signal = 'pump' if price_var >= price_variation and volume_var.abs >= volume_variation

      analyses = [
        SymMash.new(
          price_var:  price_var,
          volume_var: volume_var,
          signal:     signal,
        ),
      ]
      @result = Analysis::BaseResult.new analyses, analyser: self
      @result.signal = @result.first.signal

      @result
    end

    def report
      r = @result.first
      "Pumping at price #{r.price_var}% and volume #{r.volume_var}%"
    end

  end
end

