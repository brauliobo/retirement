module Analyser
  class Prices < PricesBase

    class_attribute :methods
    self.methods = ENV['PRICES_ANALYSERS'].split.map(&:to_sym)

    def analyse
      @result = Analysis::MultiResult.new methods.each.with_object({}) do |method, h|
        h[method] = intervals.map do |interval|
          a = pair.analyse_prices method: method, interval: interval

          n, i = a.percentages.sum.round(2), a.ipercentages.sum.round(2)
          ["#{n}%", "#{i}%"]
        end
      end
    end

    def print_report
      @result.each do |method, r|
        super method: method do
          puts "\t #{r.map(&:first).join ' / '} \t Inverted: #{r.map(&:second).join ' / '}"
        end
      end
    end

  end
end

