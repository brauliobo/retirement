class HttpClient < HTTPClient

  class_attribute :http_timeout
  self.http_timeout = ENV['HTTP_TIMEOUT'].to_i

  def initialize
    super
    self.agent_name      = UserAgents.rand
    self.connect_timeout = http_timeout/2
    self.receive_timeout = http_timeout*2
    self.send_timeout    = http_timeout
  end

end
