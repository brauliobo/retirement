class Notifier

  class_attribute :expire_time
  self.expire_time = ENV['NOTIFIER_EXPIRE_SECS'].to_i

  def self.notify summary:, body: nil, urgency: :normal, expire_time: self.expire_time.seconds
    options = {
      u: urgency,
      t: expire_time*1000,
    }
    options = options.map{ |k,v| "-#{k} \"#{v}\"" }.join ' '

    system "notify-send #{options} \"#{summary}\" \"#{body}\""
  end

end
