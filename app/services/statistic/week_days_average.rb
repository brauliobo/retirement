module Statistic
  class WeekDaysAverage < Analyser::PricesBase

    def initialize pair: nil
      super(
        pair: pair,
        days: Pair.prices_fetch_days,
      )
    end

    def generate
      @result = @prices.all.each_slice(4).with_object(Hash.new 0) do |prices, h|
        d     = prices.first.time.strftime '%a'
        h[d] += prices.last.close.growth prices.first.close
      end
    end

  end
end
