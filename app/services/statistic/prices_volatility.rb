module Statistic
  class PricesVolatility < Analyser::PricesBase

    class_attribute :intervals
    self.intervals = ENV['VOLATILITY_INTERVALS'].split

    def initialize pair: nil
      self.periods = self.class.periods.each.with_object({}) do |p, h|
        h[p] = ChronicDuration.parse(p).seconds.ago
      end
      days         = periods.map{ |p,s| ((Time.now - s) / 1.day).to_i }.max

      super(
        pair:     pair,
        interval: :'1d',
        days:     days,
      )
    end

    def generate
      @result = SymMash.new{ |h,k| h[k] = Hash.new }
      periods.each.with_object(@result) do |(period, start), h|
        ds    = prices.where{ time >= start }
        max   = ds.max :close
        min   = ds.min :close

        h.volatility[period]    = max.growth min
        h.variation[period]     = ds.last.close.growth ds.first.close
        h.min_variation[period] = ds.last.close.growth min
        h.max_variation[period] = ds.last.close.growth max
        h[min][period]          = min
        h[max][period]          = max
      end
    end

    def report
      <<-EOS
\t volatiliy:     #{@result.each{ |r| "#{r.period}: #{r.volatility}%" }.join "\t"}
\t variation:     #{@result.each{ |r| "#{r.period}: #{r.variation}%" }.join "\t"}
\t min variation: #{@result.each{ |r| "#{r.period}: #{r.min_variation}%" }.join "\t"}
\t max variation: #{@result.each{ |r| "#{r.period}: #{r.max_variation}%" }.join "\t"}
      EOS
    end

  end
end

