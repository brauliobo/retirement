module Learner
  ##
  # Machine learner for Analyser::LocalAverages
  #
  # Example console usage:
  #   l=Learner::LocalAverages.new pair: (p=Pair.find(name: 'BTC_GNO'))
  #   l.analyser.analyse; l.analyser.print_report rows: 1000
  #   l.result
  #
  class LocalAverages < AveragesBase

    self.type   = :analyser
    self.name   = :local_averages
    self.target = ::Analyser::LocalAverages

    init_params

  end
end
