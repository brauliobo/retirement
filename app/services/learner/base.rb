module Learner
  class Base

    CONFIG = SymMash.new YAML.load File.read 'config/learner.yml'

    class_attribute :type
    class_attribute :name
    class_attribute :target

    class_attribute :params

    attr_reader :pair
    attr_accessor :gains
    attr_accessor :losses

    def self.run_all
      Broker.all.each do |broker|
        broker.pairs.cpu_peach do |pair|
          learner = new pair: pair
          learner.learn
          learner.apply
          learner.save
        end
      end
    end

    def self.reset_all
      Broker.all.each do |broker|
        broker.pairs.cpu_peach do |pair|
          next unless pair[:"#{type}_params"]
          pair[:"#{type}_params"].delete name.to_s
          pair.save
        end
      end
    end

    def initialize pair:, **others
      @pair = pair
      self.params = self.params.deep_dup
      load
    end

    def skip?
      false
    end

    def learn
      raise 'not implemented'
    end

    def result
      SymMash.new(
        done_at: Time.now,
        gains:   self.gains,
        params:  params.each.with_object({}) do |(p, opts), h|
          h[p] = opts.optimal
        end,
      )
    end

    def load
      return if type_params.blank?
      type_params[name]&.params&.each do |p, v|
        params[p]&.optimal = v
      end
    end

    def save
      type_params[name.to_s] = result
      @pair.save
    end

    def type_params
      @pair[:"#{type}_params"] ||= {}
    end

    def apply
      params.each do |p, opts|
        target.send "#{p}=", opts.optimal
      end
    end

  end
end
