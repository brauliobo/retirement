module Learner
  class AveragesAlignment < AveragesBase

    self.type   = :analyser
    self.name   = :averages_alignment
    self.target = ::Analyser::AveragesAlignment

    init_params

  end
end
