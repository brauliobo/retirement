module Learner
  class AveragesBase < Base

    attr_reader :analyser

    def self.init_params
      self.params = CONFIG.send self.name
      self.params.each do |p, opts|
        next if opts[:values]
        values  = []
        current = opts[:min]
        while current <= opts[:max]
          values << current.round(2)
          current += opts.increment
        end
        opts[:values] = values
        opts.optimal  = target.send p
      end
    end

    def initialize pair:, **params
      super
      self.target = @analyser = self.class.target.new pair: pair, skip_long: true, **params
    end

    def skip?
      return true if @pair.buys.unsold.first
      return false unless last_done_at = @pair.analyser_params&.send(name)&.done_at
      return false if File.mtime('config/learner.yml') > last_done_at
      return false if Daemons::Learner.poll_interval.seconds.ago > last_done_at
      return true
    end

    def learn
      max_gains   = Integer::MIN
      min_losses  = Integer::MAX
      params.each do |p, opts|
        opts[:values].each do |v|
          @analyser.send "#{p}=", v
          @analyser.analyse

          gains  = @analyser.result.gains
          losses = @analyser.result.losses

          if gains > max_gains
            max_gains,min_losses = gains,losses
            opts.optimal = v
          end
        end

        @analyser.send "#{p}=", opts.optimal
      end

      self.losses = min_losses
      self.gains  = max_gains
    end
  end
end
