class Bovespa::Fundamentus

  FAILING = %w[]

  def initialize code
    @code = code
    @http = Capybara.current_session
    #@http.agent.user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'
  end

  def fetch
    url   = "http://www.fundamentus.com.br/detalhes.php?papel=#{@code}"
    #@page = @http.get url
    @http.visit url
    @page = @http
    @page = Nokogiri::HTML.parse @page.body

    return puts "#{@code}: not found" if @page.at 'h1:contains("Nenhum papel encontrado")'
    if @page.to_html.index 'Estamos passando por problemas'
      raise Mechanize::ResponseCodeError, SymMash.new(code: 500)
    end

    r = SymMash.new(
      code:     @code,
      nome:     cell_value('Empresa'),
      valor:    cell_value('Cotação').gsub(',', '.').to_f,
      VPA:      cell_value('VPA').gsub(',', '.').to_f,
      LPA:      cell_value('LPA').gsub(',', '.').to_f,
      balanco:  cell_value('balanço processado'),
      div_patr: cell_value('Div Br/ Patrim').gsub(',', '.').to_f,
    )
    r.lucro_percent = r.LPA/r.valor
    puts "registered #{r.inspect}"

    r
  rescue Mechanize::ResponseCodeError => e
    return if @code.in? FAILING
    puts "#{@code}: retrying HTTP #{e.response_code}, #{url}"
    sleep rand 1..3
    retry
  rescue => e
    puts "#{@code}: #{e.class} '#{e.message}' for #{url}"
    require'pry';binding.pry if ENV['DEBUG']
  end

  def cell_value title
    @page.at("td.label:has(.txt:contains('#{title}')) + td.data").text
  end

end
