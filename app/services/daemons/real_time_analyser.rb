module Daemons
  class RealTimeAnalyser < Base

    self.name = :real_time_analyser

    def self.start
      run do
        brokers = Broker.all
        brokers.each do |broker|
          broker.ticker_subscribe receive: method(:receive_ticker)
        end
      end
    end

    def self.receive_ticker ticker
      pair  = ticker.pair
      return unless pair.accepted?

      c = ActionCable.server.broadcast "tickers/#{pair.broker}/#{pair.name}", ticker
      #pp ticker if c > 0

      CatchBlock.exec do
        analyse pair: pair, ticker: ticker
      end
    end

    def self.analyse pair:, ticker:
      price = PairPrice.new(
        broker: pair.broker,
        pair:   pair.name,
        time:   Time.now,
        open:   ticker.last_price,
        high:   ticker.last_price,
        low:    ticker.last_price,
        close:  ticker.last_price,
      )

      prices = pair.prices.days(7).all + [price]
      sim    = Daemons::Simulator.new

      buy = Buy.pair(pair).unsold.first
      if buy
        analysis = Interest::SellFinder.signal_interest? pair: pair, buy: buy, prices: prices
        sim.sell analysis: analysis, buy: buy if analysis
      else
        analysis = Interest::BuyFinder.signal_interest? pair: pair, prices: prices
        sim.buy analysis: analysis if analysis
      end
    end

  end
end
