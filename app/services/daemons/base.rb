module Daemons
  class Base

    class_attribute :name
    self.name = :unnamed

    class_attribute :poll_interval
    self.poll_interval = ENV['DAEMON_POLL_INTERVAL'].to_i.minutes

    class_attribute :pair_interval
    self.pair_interval = ENV['DAEMON_PAIR_INTERVAL'].to_i.seconds

    def self.run
      brokers = Broker.all
      thread  = Thread.new do
        loop do
          brokers.catch_each do |broker|
            start = Time.now
            puts "#{self.name}: Started #{broker.name}"

            yield broker, data(broker: broker)

            duration = Time.now - start
            wait     = poll_interval.to_i - duration.to_i
            duration = ApplicationController.helpers.distance_of_time_in_words duration
            puts "\n#{self.name}: Finished in #{duration}. Waiting #{wait}s for the next polling\n\n"
            sleep wait
          end
        end
      end

      thread
    end

    ##
    # By default all pairs from the broker
    #
    def self.data broker:
      pairs   = load_pairs broker: broker, pairs: ENV['PAIRS']
      pairs ||= broker.pairs
    end

    def self.load_pairs broker:, pairs:
      pairs&.split(',')&.map{ |p| Pair.find_or_create broker: broker.name, name: p }
    end

  end
end
