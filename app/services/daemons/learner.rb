module Daemons
  class Learner < Base

    self.name = :learner

    self.poll_interval = self.poll_interval * 30

    class_attribute :learners
    self.learners = ENV['LEARNERS'].split.map(&:to_sym)

    def self.start
      learner = new

      run do |broker, pairs|
        pairs.cpu_peach do |pair|
          learners.each do |learner_name|
            learner.run learner: learner_name, pair: pair
          end
        end
      end
    end

    def initialize
      Broker.all.each do |broker|
        broker.pairs.each do |pair|
          learners.each do |learner|
            learner = instance_for learner: learner, pair: pair
          end
        end
      end
    end

    def run learner:, pair:
      header  = "#{name}/#{learner}: #{pair.broker}/#{pair.name}:"
      learner = instance_for learner: learner, pair: pair

      return puts "#{header} skipped" if learner.skip?

      learner.learn
      learner.save
      learner.apply
      puts "\n#{header} #{learner.result.inspect} \n\n"
    end

    protected

    def instance_for learner:, pair:
      klass   = ::Learner.const_get learner.to_s.camelize
      klass.new pair: pair
    end

  end
end
