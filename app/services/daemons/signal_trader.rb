module Daemons
  class SignalTrader < Base

    self.name = :signal_trader

    self.poll_interval = self.poll_interval / 4

    def self.start
      trader = new

      run do |broker, (pending_buys, pending_sells)|
        pending_sells.api_peach do |pending_sell|
          broker.sell pending_sell
          trader.notify_sell pending_sell
        end

        pending_buys.api_peach do |pending_buy|
          broker.buy pending_buy
          trader.notify_buy pending_buy
        end
      end
    end

    def self.data broker:
      return [] unless broker.trading_api_enabled

      buys  = broker.pending_buys
      sells = broker.pending_sells.with_buy

      [buys, sells]
    end

    def notify_buy buy
      pair = buy.find_pair
      Notifier.notify(
        summary: "#{pair.broker}/#{pair.name}",
        body:    "Bought #{buy.amount} at rate #{buy.price}",
      )
    end

    def notify_sell sell
      pair = sell.find_pair
      Notifier.notify(
        summary: "#{pair.broker}/#{pair.name}",
        body:    "Sold #{sell.amount} at rate #{sell.price}: #{sell.gain}%",
      )
    end

  end
end
