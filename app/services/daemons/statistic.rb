module Daemons
  class Statistic < Base

    self.name = :stats
    self.poll_interval = 1.hour

    class_attribute :statistics
    self.statistics = ENV['STATISTICS'].split.map(&:to_sym)

    def self.start
      d = new
      run do |broker, pairs|
        sleep poll_interval
        pairs.api_peach do |pair|
          d.generate pair: pair
        end
      end
    end

    def generate pair:
      statistics.each.api_peach do |name|
        klass = ::Statistic.const_get name.to_s.camelize
        stat  = klass.new pair: pair
        pair.statistics[name] = stat.generate
      end
      pair.save
    end

  end
end
