module Daemons
  class Simulator < Base

    self.name = :simulator

    def self.start
      sim = new

      run do
        sim.buy_all
        sim.sell_all

        summary = Analysis::Trades.summary
        ratio   = summary.map{ |h| h[:price_ratio] }.sum.round 2
        puts "\nsimulator: summary: #{summary.count} trades, #{ratio}% of gain/loss"
      end
    end

    def buy_all
      analyses = Interest::BuyFinder.find
      analyses.map do |analysis|
        buy analysis: analysis
      end
    end

    def sell_all
      analyses = Interest::SellFinder.find
      analyses.map do |analysis, buy|
        sell analysis: analysis, buy: buy
      end
    end

    def buy analysis:
      pair = analysis.find_pair
      sync pair: pair

      decision = Volume::PerOrders.calculate_buy pair: pair
      buy      = Buy.create(
        broker:     pair.broker,
        pair:       pair.name,
        pending:    false,
        simulated:  true,
        price_id:   analysis.price_id,
        price:      decision.price,
        signal_id:  analysis.signal_id,
        amount:     decision.volume,
        time:       Time.now,
      )

      puts "simulator: #{pair.broker}/#{pair.name}: Buy #{decision.symbolize_keys.inspect}"

      buy
    end

    def sell analysis:, buy:
      pair = analysis.find_pair
      sync pair: pair

      decision = Volume::PerOrders.calculate_sell buy: buy
      sell     = Sell.create(
        broker:     pair.broker,
        pair:       pair.name,
        pending:    false,
        simulated:  true,
        buy_id:     buy.id,
        price_id:   analysis.price_id,
        price:      decision.price,
        signal_id:  analysis.signal_id,
        amount:     decision.volume,
        time:       Time.now,
      )
      buy.update sell_id: sell.id

      puts "simulator: #{pair.broker}/#{pair.name}: Sell #{decision.symbolize_keys.inspect}"

      sell
    end

    protected

    def sync pair:
      Syncer::Orders.new.sync pair: pair
    end

  end
end
