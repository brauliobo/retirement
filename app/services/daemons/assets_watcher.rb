module Daemons
  class AssetsWatcher < Base

    self.name = :assets_watcher

    self.poll_interval = self.poll_interval * 8

    def self.start
      watcher = new

      run do |broker, balances|
        watcher.notify broker: broker, balances: balances
      end
    end

    ##
    # Base data to be used at each #run interation above
    #
    def self.data broker:
      return {} unless broker.trading_api_enabled

      broker.balances.each.with_object({}) do |(pair, data), h|
        pair    = ::Pair.find_or_create broker: broker.name, name: pair
        h[pair] = data
      end
    end

    def notify broker:, balances:
      body = balances.map do |pair, data|
        "#{pair.name}: variation #{data.btcValue.to_f.growth data.buyValue}%"
      end.join '<br>'
      Notifier.notify(
        summary: "#{broker.name} balances",
        body:    body,
      )
    end

  end
end
