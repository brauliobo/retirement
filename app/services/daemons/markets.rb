module Daemons
  class Markets < Base

    self.name = :markets

    def self.start
      run do
        fetch_coins
        fetch_market_prices
      end
    end

    def self.fetch_coins
      return unless ::Coin.count.zero?

      ::Coin.dataset.insert_conflict.multi_insert Coinmarketcap::Coins.fetch
      ::Coin.all
    end

    def self.fetch_market_prices
      ::Coin.each do |coin|
        next if coin.symbol == ::Broker::Base.base_coin.to_s

        markets = Coinmarketcap::Markets.fetch id: coin.coinmarketcap_id
        markets.each do |m|
          ::PairPrice.create(
            broker:   m.broker,
            pair:     m.pair,
            time:     Time.now,
            volume:   m.volume,
            close:    m.price,
            interval: m.interval,
          )
        end
      end
    end

  end
end
