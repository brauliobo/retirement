module Daemons
  class Analyser < Base

    class_attribute :analysers
    self.analysers = ENV['ANALYSERS'].split.map(&:to_sym)

    def self.start
      raise 'Use syncer daemon ATM'
    end

    def initialize
    end

    def analyse pair:
      l = []
      analysers.each.cpu_peach do |name|
        r = run name: name, pair: pair
        l.push r
      end

      l.each do |r|
        next unless r.signal

        puts "\n"
        if r.analyser.report?
          r.analyser.print_header
          r.analyser.print_report
        end
        r.analyser.notify if r.analyser.notify?
        puts "\n"

        pair.buy_from_signal  analysis: r if r.buy_signal
        pair.sell_from_signal analysis: r if r.sell_signal
      end
    end

    def run name:, pair:
      klass    = ::Analyser.const_get name.to_s.camelize
      analyser = klass.new pair: pair

      analyser.analyse
    end

  end
end
