module Daemons
  class Syncer < Base

    self.name = :syncer

    class_attribute :syncers
    self.syncers = ENV['SYNCERS'].split.map(&:to_sym)

    #self.poll_interval = 0

    def self.start
      syncer   = new
      analyser = Daemons::Analyser.new

      run do |broker, pairs|
        pairs.api_peach do |pair|

          syncers.each do |syncer_name|
            puts "#{name}/#{syncer_name}: #{pair.broker}/#{pair.name}"
            syncer.send "sync_#{syncer_name}", pair: pair
            sleep pair_interval
          end

          Thread.new do
            analyser.analyse pair: pair
          end
        end
      end
    end

    def sync_prices pair:
      prices_syncer = ::Syncer::Prices.new
      prices_syncer.sync_short_term pair: pair
      prices_syncer.sync_long_term  pair: pair
    end

    def sync_orders pair:
      orders_syncer = ::Syncer::Orders.new
      orders_syncer.sync pair: pair
    end

  end
end
