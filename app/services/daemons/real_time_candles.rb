module Daemons
  class RealTimeCandles < Base

    self.name = :real_time_candles

    def self.start
      daemon = new

      brokers = Broker.all
      brokers.each do |broker|
        broker.ticker_subscribe receive: daemon.method(:receive_ticker)
      end

      daemon
    end

    def receive_ticker ticker
      pair = ticker.pair
      pp pair

    end

  end
end
