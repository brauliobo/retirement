module Daemons
  ##
  # Create pending sell manually:
  #   Sell.create broker: 'poloniex', pair: 'BTC_DASH', pending: true, price: 0.071
  #
  # Or for every balance
  #   Broker.poloniex.balances.each{ |p, b| s=Sell.create broker: 'poloniex', pair: p, pending: true, price: b.buyPrice*1.02 }
  #
  class Seller < Base

    self.name = :seller

    self.poll_interval = self.poll_interval

    def self.start
      watcher = new

      run do |broker, pending_sells|
        pending_sells.api_peach do |pending_sell|
          pair = pending_sell.find_pair

          broker.sell pending_sell

          watcher.notify pair: pair, sell: pending_sell unless pending_sell.pending
        end
      end
    end

    ##
    # Base data to be used at each #run interation above
    #
    def self.data broker:
      return [] unless broker.trading_api_enabled

      broker.pending_sells.where(analyser: nil)
    end

    def notify pair:, sell:
      Notifier.notify(
        summary: "#{pair.broker}/#{pair.name}",
        body:    "Sold",
      )
    end

  end
end
