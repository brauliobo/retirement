module Daemons
  class Controller

    class_attribute :daemons
    self.daemons = []

    def self.start
      load_list
      start_daemons
      sleep 999.days
    end

    def self.load_list
      self.daemons = ENV['DAEMONS'].split.map do |d|
        Daemons.const_get d.camelize.to_sym
      end
    end

    def self.start_daemons
      daemons.peach threads: daemons.size do |daemon|
        puts "daemon/#{daemon.name}: start"
        daemon.start
      end
    end

  end
end
