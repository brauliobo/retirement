module Interest
  class BuyFinder

    class_attribute :analysis_use_inverted
    self.analysis_use_inverted = ENV['ANALYSIS_USE_INVERTED'].in? %w[true 1]

    class_attribute :prices_minimum_percentage
    self.prices_minimum_percentage = ENV['PRICES_MINIMUM_PERCENTAGE'].to_i

    class_attribute :buy_method
    self.buy_method = ENV['BUY_METHOD']

    def self.find method: buy_method, pairs: Pair.active, **params
      analyses = []

      pairs.cpu_peach do |pair|
        analysis = send method, pair: pair, **params
        next unless analysis

        analyses << analysis
      end

      analyses
    end

    def self.signal_interest? pair:, prices: nil, signal_id: 'up', bars: 1, **params
      analyses = pair.analyse_prices prices: prices, **params

      analysis = analyses.last(bars).find{ |p| p.signal == signal_id }
      return unless analysis

      # coins with very low price/precision have too much up and down signals
      return if analysis.close.round(5).zero?

      current_buys = Buy.unsold.pair(pair)
      return if current_buys.first
      #return if current_buys.where(price_id: analysis.price_id).first

      analysis.signal_id = signal_id
      analysis
    end

    def self.analysis_results? pair:, prices: nil, minimum_percentage: prices_minimum_percentage, use_inverted: analysis_use_inverted
      s  = pair.analyse_prices prices: prices
      ns = s.percentages.sum
      is = s.ipercentages.sum

      if use_inverted and is > minimum_percentage
        signal_id = 'down'
      elsif ns > minimum_percentage
        signal_id = 'up'
      else
        return
      end

      signal_interest? pair: pair, prices: prices, signal_id: signal_id
    end

    def self.lowest_price? pair:, prices: nil, minimum_percentage: prices_minimum_percentage, use_inverted: analysis_use_inverted
      Analysis::Pairs.lowest_prices.each do

      end
    end

  end
end
