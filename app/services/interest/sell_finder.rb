module Interest
  class SellFinder

    def self.find method: :signal_interest?, buys: Buy.unsold.simulated.all, **params
      analyses = {}

      buys.cpu_peach do |buy|
        pair     = buy.find_pair
        analysis = send method, pair: pair, buy: buy, **params
        next unless analysis

        analyses[analysis] = buy
      end

      analyses
    end

    def self.signal_interest? pair:, buy:, prices: nil, bars: 1, **params
      analyses  = pair.analyse_prices prices: prices, **params
      signal_id = if buy.signal_id == 'down' then 'up' else 'down' end

      analysis  = analyses.last(bars).find{ |p| p.signal == signal_id }
      return unless analysis

      analysis.signal_id = signal_id
      analysis
    end

  end
end
