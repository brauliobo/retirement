class Bovespa

  def initialize file
    @codes = File.readlines(file).each{ |x| x.chomp! }
  end

  def fetch_all
    data = []
    @codes.peach do |code|
      d = Fundamentus.new(code).fetch
      next unless d
      data << d
    end

    CSV.open 'fundamentus.csv', 'wb' do |csv|
      csv << data.first.keys
      data.each{ |d| csv << d.values }
    end
  end

end

