module Syncer
  class Orders

    def self.sync pair:
      new.sync pair: pair
    end

    def initialize
    end

    def sync pair: Pair.default
      data = pair.broker_module::OrderBook.fetch pair: pair

      DB.transaction do
        PairAsk.dataset.insert_conflict.multi_insert data.to_asks
        PairBid.dataset.insert_conflict.multi_insert data.to_bids
      end
    end

  end
end
