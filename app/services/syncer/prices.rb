module Syncer
  class Prices

    def sync_long_term pair:, days: Pair.prices_long_fetch_days, interval: Pair.prices_long_interval
      prices = sync_interval(
        pair:          pair,
        interval:      interval,
        default_start: days.days.ago.beginning_of_day,
      )
      save_variation pair: pair, prices: prices

      prices
    end

    def sync_short_term pair:, days: Pair.prices_fetch_days, interval: Pair.prices_interval
      sync_interval(
        pair:          pair,
        interval:      interval,
        default_start: days.days.ago.beginning_of_hour,
      )
    end

    def sync_interval pair:, interval:, default_start:
      prices     = pair.prices interval: interval
      last_price = prices.last
      start_time = if last_price then last_price.time else default_start end

      up_to_date = start_time + (Interval::SEC_MAP[interval] / 60) >= Time.now
      return prices if up_to_date

      PairPrice.dataset.insert_conflict.multi_insert data.to_pair_prices

      prices
    end

    def save_variation pair:, prices:
      pair.update(
        last_price:          prices.last.close,
        lowest_price_in_3m:  prices.where{ time >= 3.months.ago }.min(:low),
        lowest_price_in_6m:  prices.where{ time >= 6.months.ago }.min(:low),
        lowest_price_in_12m: prices.where{ time >= 12.months.ago }.min(:low),
      )
    end

  end
end
