module Volume
  class PerOrders

    include Signals::Availability

    ##
    # @param max_volume_percentage max percentage of 24h volume
    # @param max_price_increase max percentage of price to tolerate
    #
    def self.calculate_buy pair:, max_volume_percentage: 3, max_price_increase: 2

      calc = SymMash.new
      asks = pair.asks.to_a

      max_daily_volume = pair.prices.volume * max_volume_percentage.to_f/100
      calc.max_volume  = if max_daily_volume < max_available_to_buy then max_daily_volume else max_available_to_buy end
      calc.max_price   = pair.first_ask.price * (1+max_price_increase.to_f/100)

      asks.each.with_index.inject 0 do |volume_for_price, (ask, i)|
        volume_for_price += ask.base_amount

        i_asks       = asks[0..i]
        calc.price   = i_asks.inject(0){ |m, a| m += a.price * a.base_amount } / volume_for_price

        calc.criteria = :volume if volume_for_price > calc.max_volume
        calc.criteria = :price  if calc.price       > calc.max_price

        if calc.criteria
          calc.volume = if volume_for_price < calc.max_volume then volume_for_price else calc.max_volume end

          calc.price_ratio    = calc.price / pair.first_ask.price
          calc.order_position = i+1

          break
        end

        volume_for_price
      end

      calc
    end

    ##
    #
    def self.calculate_sell buy:
      pair = buy.find_pair
      calc = SymMash.new
      bids = pair.bids.all

      calc.buy_price = buy.price

      bids.each.with_index.inject 0 do |volume_for_price, (bid, i)|
        volume_for_price += bid.base_amount

        if volume_for_price >= buy.amount
          i_bids          = bids[0..i]
          calc.sell_price = i_bids.inject(0){ |m, b| m += b.price * b.base_amount } / volume_for_price

          calc.profit_percentage = calc.sell_price.growth calc.buy_price
          calc.order_position    = i+1

          calc.price  = calc.sell_price
          calc.volume = volume_for_price

          break
        end

        volume_for_price
      end

      calc
    end

  end
end
