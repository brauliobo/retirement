module Trader
  class Analyses

    def self.orders
      buy_signal,sell_signal = 'up','down'

      buy_ds = PairOrderAnalysis
        .where(signal: buy_signal)

      buy_ds.each do |ba|
        pair    = ba.find_pair
        sell_ds = PairOrderAnalysis
          .pair(pair)
          .where(signal: sell_signal)
          .where{ fetched_at > ba.fetched_at }
          .all
        next if sell_ds.blank?

        sa = sell_ds.min_by do |sa|
          buy_price  = ba.ask_price
          sell_price = sa.bid_price
          sell_price.growth buy_price
        end

        buy_price  = ba.ask_price
        sell_price = sa.bid_price
        puts "trader/analyses: #{pair.broker}/#{pair.name}: sell ratio #{sell_price.growth buy_price}%"
      end

    end

  end
end
