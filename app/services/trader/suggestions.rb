module Trader
  class Suggestions

    METHODS = {
      min_variation:  -> v { 4 * ((-v)/100) },
      max_volatility: -> v { 3 * ((+v)/100) },
      max_buy_ratio:  -> v { 2 * ((+v)/100) },
      min_price:      -> v { 1 * ((-v)/100) },
    }

    def self.summary
      r = METHODS.each.with_object({}) do |(m, rank), h|
        send(m).each do |p, v|
          name         = m.to_s.split('_', 2).second
          h[p]       ||= SymMash.new pair: p, rank: 0
          h[p][name]   = v
          h[p].rank   += rank.call v
        end
      end.values
      r.sort_by!{ |p| -p.rank }

      tp r.first(10)

      r
    end

    def self.max_volume_increase
      # TODO:
    end

    def self.max_buy_ratio
      pairs = Pair.active.all

      r = pairs.each.with_object({}) do |p, h|
        buy_ratio = PairOrderAnalysis
          .pair(p)
          .latest
          .limit(Analyser::MarketDepth.max_order_position)
          .order(Sequel.asc :ask_id)
          .max(:bid_ask_ratio)
        next unless buy_ratio

        h[p.name] = buy_ratio
      end

      Hash[r.sort_by{ |p,v| -v }]
    end

    def self.min_variation
      pairs = Pair.active.all

      r = pairs.each.with_object({}) do |p, h|
        a = p.analyse_volatility

        h[p.name] = a.min_by(&:variation).variation
      end

      Hash[r.sort_by{ |p,v| v }]
    end

    def self.min_price
      pairs = Pair.active.all.select{ |p| p.last_price }

      r = pairs.each.with_object({}) do |p, h|
        min_price = [p.lowest_price_in_3m, p.lowest_price_in_6m, p.lowest_price_in_12m].min

        h[p.name] = p.last_price.growth(min_price)
      end

      Hash[r.sort_by{ |p,v| v }]
    end

    def self.max_volatility
      pairs = Pair.active.all

      r = pairs.each.with_object({}) do |p, h|
        a = p.analyse_volatility

        h[p.name] = a.max_by(&:volatility).volatility
      end

      Hash[r.sort_by{ |p,v| -v }]
    end

  end
end
